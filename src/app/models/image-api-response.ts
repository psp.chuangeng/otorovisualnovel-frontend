export interface Image {
    name: string;
    url: string;
}

export interface ImageApiResponse {
    user: string;
    imageType: string; 
    images: Image[];
}


export interface getImagesS3Response{
    url: string[] | string;
    statusCode: number; 
}