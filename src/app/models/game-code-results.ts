//declare module namespace {

    export interface PlayerScore {
        attributeName: string;
        score: string;
    }

    export interface Player {
        playerName: string;
        playerScore: PlayerScore[];
    }

    export interface RootObject {
        gameid: string;
        players: Player[];
    }

//}
