export interface PlayerScore {
    attributeName: string;
    score: number;
}

export interface GameResultPost {
    gameCode?: string;
    playerName: string;
    playerScores: PlayerScore[];
}


