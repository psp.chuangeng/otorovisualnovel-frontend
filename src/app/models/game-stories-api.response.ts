export interface Game {
    id: number;
    storyTitle: string;
    gameCodes: number[];
}


export interface GameStoriesResponse {
    game: Game[];
}

