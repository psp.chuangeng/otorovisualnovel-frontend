export interface StoryApiResponse{
    userID : number;
    stories: storyList[];
}

export interface storyList{
    storyID: number;
    storyTitle: string;
}