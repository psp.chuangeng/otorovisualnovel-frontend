export interface Attribute {
    meterId?: number;
    attributeName: string;
    attributeImage: string;
    winningScore: number;
    attributeDescription: string;
}

export interface Consequence {
    attributeName: string;
    attributeImage?: string; //for scenario builder usage only
    effect: number;
}

export interface Option {
    optionId: string;
    optionName: string;
    impactScene? : string,
    consequences?: Consequence[];
}

export interface Scene {
    sceneId?: string;
    stackNo : number;
    sceneType: string;
    backgroundImage: string;
    characterName: string;
    characterImage: string;
    graphType?: string;
    graphImage: string;
    graphData: string;
    description: string;
    options: Option[];
}

export interface GameApiResponse { //main usage for builder 
    userID?: number;
    storyId?: string;
    storyTitle: string;
    attributes: Attribute[]; //meter 
    scenes: Scene[];
}