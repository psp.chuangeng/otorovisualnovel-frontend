import { HTTP_INTERCEPTORS, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { TokenStorageService } from '../services/token-storage.service';

import { Observable } from 'rxjs';

const TOKEN_HEADER_KEY = 'Authorization';       // for Spring Boot back-end


// HttpInterceptor has intercept() method to inspect and transform HTTP requests before they are sent to server.


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private token: TokenStorageService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = req;
    if (req.responseType != "arraybuffer"){ //only exception for getting excel data from s3 don send over authroization headers
      const token = this.token.getToken();
      if (token != null) {
          // add Authorization header with ‘Bearer’ prefix to the token.
        authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token) });
      }
    }
    return next.handle(authReq);
  }
}

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];

/**intercept() gets HTTPRequest object, change it and forward to HttpHandler object’s handle() method.
 *  It transforms HTTPRequest object into an Observable<HttpEvents>.
next: HttpHandler object represents the next interceptor in the chain of interceptors. 
The final ‘next’ in the chain is the Angular HttpClient.
 */