// Angular in built modules 
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

//Clarity UI 
import { ClarityModule } from '@clr/angular';

//Excel data upload 
import { ReadexcelDirective } from '../directives/readexcel.directive';


//Components 
import { SceneBuilderComponent } from './builder-components/scenebuilder/scenebuilder.component';

import { StoryBuilderComponent } from './builder-components/storybuilder/storybuilder.component';

import { StorylineComponent } from './builder-components/storyline/storyline.component';
import { HostgameComponent } from './builder-components/hostgame/hostgame.component';
import { NavbarComponent } from './builder-components/navbar/navbar.component';
import { LoginComponent } from './builder-components/login/login.component';
import { ImageComponent } from './builder-components/image/image.component';
import { BuilderComponent } from './builder.component';
import { RegistrationComponent } from './builder-components/registration/registration.component';
import { ForgetpasswordComponent } from './builder-components/forgetpassword/forgetpassword.component';
import { CheckresetemailComponent } from './builder-components/checkresetemail/checkresetemail.component';

//Interactive charts components 
import { ChartComponent } from './builder-components/chart/chart.component';
import { BarComponent } from './builder-components/chart/bar/bar.component';
import { BoxComponent } from './builder-components/chart/box/box.component';
import { LinearComponent } from './builder-components/chart/linear/linear.component';
import { PieComponent } from './builder-components/chart/pie/pie.component';
import { LinearRegressionPlotComponent } from './builder-components/chart/linear-regression-plot/linear-regression-plot.component';
import { ResultsComponent } from './builder-components/results/results.component';

//Services
import {FormService} from '../services/form.service';
import { WinlosesceneComponent } from './builder-components/winlosescene/winlosescene.component';


//Proviers 
import { authInterceptorProviders } from '../helpers/auth.interceptor';
import { LineComponent } from './builder-components/chart/line/line.component';
import { ScenepreviewComponent } from './builder-components/scenepreview/scenepreview.component';
import { SceneComponent } from '../game/game-components/scene/scene.component';


const routes: Routes = [
      {path: '', component: StoryBuilderComponent},
      {path: 'image', component: ImageComponent},
      {path: 'login', component: LoginComponent},
      {path: 'registration', component: RegistrationComponent},
      //{path: 'forgetpassword', component: ForgetpasswordComponent},
      //{path: 'checkemail', component: CheckresetemailComponent},
      {path: 'result', component: ResultsComponent},
      {path: 'stories', component: StorylineComponent},
      {path: 'preview-game', component: SceneComponent},
      {path: 'preview-scene', component: ScenepreviewComponent},


  ];

@NgModule({
  declarations: [
    LinearRegressionPlotComponent,
    LinearComponent,
    PieComponent,
    BoxComponent,
    BarComponent,
    LineComponent,
    NavbarComponent,
    StoryBuilderComponent,
    SceneBuilderComponent,
    ImageComponent,
    BuilderComponent,
    LoginComponent,
    RegistrationComponent,
    ForgetpasswordComponent,
    CheckresetemailComponent,
    ReadexcelDirective,
    ChartComponent,
    ResultsComponent,
    StorylineComponent,
    HostgameComponent,
    WinlosesceneComponent,
    ScenepreviewComponent,
  ],
  imports: [
    RouterModule.forRoot(routes),
    HttpClientModule,
    ClarityModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    DragDropModule,
    BrowserAnimationsModule

  ],
  providers: [FormService, authInterceptorProviders],
  exports: [RouterModule, ChartComponent],
  bootstrap: [BuilderComponent]
})

export class BuilderRoutingModule { }
