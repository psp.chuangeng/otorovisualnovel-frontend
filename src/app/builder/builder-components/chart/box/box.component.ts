import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Subscription } from 'rxjs';
declare const Plotly:any;

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {

  @Input() data: any;
  @Input() childLabel: any;
  
  @ViewChild("boxPlotGraph", { static: false }) boxPlotContainer!: ElementRef;
  boxPlotGraph: any;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(){

    let keys = this.data[0];
    keys = Object.keys(keys);
    let test_arry: any[] = [];
    let final: any[] = [];

    for (let i = 0; i < keys.length; i++) {
      this.data.map((o: any)=> 
         test_arry.push(o[keys[i]])
        )
        final.push(test_arry);
        test_arry = []
      }

    let data_array = []; 

    for (let i = 0; i < keys.length; i++) {
      data_array.push(keys[i] = {
      name: keys[i],
      y : final[i],
      type: 'box'
      })
    }
    //console.log(data_array);

    this.boxPlotGraph = {
      data : 
        data_array
  }

  let layout = {
    title: this.childLabel['boxPlot']
  };

    Plotly.newPlot(
      this.boxPlotContainer.nativeElement,
      this.boxPlotGraph.data,
      layout,
      //this.boxPlotGraph.layout,
      this.boxPlotGraph.config
    );


  }

}
