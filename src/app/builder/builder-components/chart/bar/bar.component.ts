import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Subscription } from 'rxjs';
declare const Plotly:any;


@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.css']
})
export class BarComponent implements OnInit {

  @Input() data: any;
  @Input() childLabel: any;


  @ViewChild("barGraph", { static: false }) barGraphContainer!: ElementRef;
  labels?: string[];
  data_sum?: number[];
  barGraph: any;

  constructor() { }
  
  ngOnInit(): void {
    console.log(this.childLabel['barChart'])

  }

  ngAfterViewInit(){

    const result = this.data.reduce((sums: number, obj: any) => Object.keys(obj).reduce((s: any, k: any) => {    
      k === 'id' || (s[k] = (s[k] || 0) + +obj[k]);
      return s;
      }, sums), {});
      this.labels = Object.keys(result);
      this.data_sum = Object.values(result);

    this.barGraph = {
      data: [
        {
          x: this.labels,
          y: this.data_sum,
          type: 'bar',
          //name: 'Weekend Rides',
        },
        //{
        //  x: [1, 2, 3],
        //  y: [2, 5, 6],
        //  type: 'scatter',
        //  name: 'Week Day Rides',
        //},
      ],
      layout: {
        responsive: true,
        title: { text: this.childLabel['barChart'] },
        xaxis: { 
          //title: { text: "Hour of the day" },
        },
        yaxis: {
          //title: { text: "Rides" },
        },
      }
    };

    Plotly.newPlot(
      this.barGraphContainer.nativeElement,
      this.barGraph.data,
      this.barGraph.layout,
      this.barGraph.config
    );

  }



}
