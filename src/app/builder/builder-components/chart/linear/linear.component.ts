import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from "@angular/cdk/drag-drop";
declare const Plotly:any;


@Component({
  selector: 'app-linear',
  templateUrl: './linear.component.html',
  styleUrls: ['./linear.component.css']
})
export class LinearComponent implements OnInit {

  @Input() data: any;
  @ViewChild("linearGraph", { static: false }) linearGraphContainer!: ElementRef;

  linearNameData: any[];
  linearGraph: any; 
  show:boolean;
  x_data: any[] = [];
  y_data: any[] = [];
  hide?: string; 


  constructor() { 
    this.show = false;  
    //this.hide = 'style="hide"';
    this.linearNameData = [];
  }

  ngOnInit(): void {
    this.show = !this.show; //show the drag and drop 
    let keys = this.data[0]; //get all keys - first row of excel 

    keys = Object.keys(keys);

    let test_arry: any[] = [];
    let final: any[] = [];

    for (let i = 0; i < keys.length; i++) {
      this.data.map((o: any) => 
         test_arry.push(o[keys[i]])
        )
        final.push(test_arry);
        test_arry = []
      }

    let data_array = []; 

    for (let i = 0; i < keys.length; i++) {
      data_array.push(keys[i] = {
      name: keys[i],
      y : final[i],
      type: 'box'
      })

    }
    this.linearNameData = data_array;
  }

  ngAfterViewInit(){
    
  }

  replot(){
    this.show = !this.show; //show the drag and drop
  }

  linearRegression(x: number[],y: number[]){

    let lr = {'sl': 0, 'off': 0, 'r2': 0};
    let n = y.length;
    let sum_x = 0;
    let sum_y = 0;
    let sum_xy = 0;
    let sum_xx = 0;
    let sum_yy = 0;

    for (let i = 0; i < y.length; i++) {

        sum_x += x[i];
        sum_y += y[i];
        sum_xy += (x[i]*y[i]);
        sum_xx += (x[i]*x[i]);
        sum_yy += (y[i]*y[i]);
    } 

    lr['sl'] = (n * sum_xy - sum_x * sum_y) / (n*sum_xx - sum_x * sum_x);
    lr['off'] = (sum_y - lr.sl * sum_x)/n;
    lr['r2'] = Math.pow((n*sum_xy - sum_x*sum_y)/Math.sqrt((n*sum_xx-sum_x*sum_x)*(n*sum_yy-sum_y*sum_y)),2);

    return lr;
}

displayLinearChart(x_data: any[], y_data: any[]){
  this.show = !this.show; //show the drag and drop 

  let trace = {
    x: x_data[0].y, // which data to use data_array[0].y 
    y: y_data[0].y, //which data to use data_array[1].y
    name: 'X: ' + x_data[0].name  + ' vs ' + 'Y: ' + y_data[0].name,
    "marker": {"size": 5},
    "mode": "markers",
    "type": "scatter" };  

  let lr = this.linearRegression(trace.x, trace.y);
  let fit_from = Math.min(...trace.x)
  let fit_to = Math.max(...trace.x)
  let fit = {
    x: [fit_from, fit_to],
    y: [fit_from*lr.sl+lr.off, fit_to*lr.sl+lr.off],
    mode: 'lines',
    type: 'scatter',
    name: "R2=".concat((Math.round(lr.r2 * 10000) / 10000).toString())
  };

  this.linearGraph = {
    data: [ trace, fit ] 
  }

  Plotly.newPlot(
    this.linearGraphContainer.nativeElement,
    this.linearGraph.data,
    this.linearGraph.layout,
    this.linearGraph.config
  );
}


drop(event: CdkDragDrop<number[]>) {
  if (event.previousContainer === event.container) {
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  } else {
    transferArrayItem(
      event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  }
}

}
