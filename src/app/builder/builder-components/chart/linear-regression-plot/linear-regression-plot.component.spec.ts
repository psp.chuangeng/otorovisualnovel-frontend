import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinearRegressionPlotComponent } from './linear-regression-plot.component';

describe('LinearRegressionPlotComponent', () => {
  let component: LinearRegressionPlotComponent;
  let fixture: ComponentFixture<LinearRegressionPlotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LinearRegressionPlotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LinearRegressionPlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
