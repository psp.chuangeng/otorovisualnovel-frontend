import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from "@angular/cdk/drag-drop";
declare const Plotly:any;


@Component({
  selector: 'app-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.css']
})
export class LineComponent implements OnInit {

  @Input() data: any;
  @Input() childLabel: any;
  
  @ViewChild("lineGraph", { static: false }) lineContainer!: ElementRef;
  show:boolean;
  lineNameData: any[]; 
  x_data: any[] = [];
  y_data: any[] = [];
  lineGraph: any[] = [];
  //lineGraph: any; 



  constructor() { 
    this.show = false;  
    //this.hide = 'style="hide"';
    this.lineNameData = [];
  }

  ngOnInit(): void {

    this.show = !this.show; //show the drag and drop 
    let keys = this.data[0]; //get all keys - first row of excel 

    keys = Object.keys(keys);

    let test_arry: any[] = [];
    let final: any[] = [];

    for (let i = 0; i < keys.length; i++) {
      this.data.map((o: any) => 
         test_arry.push(o[keys[i]])
        )
        final.push(test_arry);
        test_arry = []
      }

    let data_array = []; 

    for (let i = 0; i < keys.length; i++) {
      data_array.push(keys[i] = {
      name: keys[i],
      y : final[i],
      type: 'scatter'
      })

    }
    this.lineNameData = data_array;

    console.log(this.lineNameData)

  }

  
  replot(){
    this.show = !this.show; //show the drag and drop
    this.lineGraph = [];
  }

  displayLineChart(x_data: any[], y_data: any[]){
    this.show = !this.show;

    console.log('x', x_data);
    console.log('y', y_data);

    let yaxisLabels: any[] = [];
    y_data.forEach((data: any) => {

      //console.log({
      //  x : x_data[0].y,
      //  y: data.y,
      //})

      //data.name // if need name for labels

      yaxisLabels.push(data.name)
       
      this.lineGraph.push({
        x : x_data[0].y.sort((a: number,b : number) => a-b), // always the same x data
        y:  data.y.sort((a: number,b : number) => a-b), //iterate y data 
        type: data.type, 
        name: data.name
      })

    })

    //Plotly.newPlot(
    //  this.lineContainer.nativeElement,
    //  this.lineGraph.data,
    //  this.lineGraph.layout,
    //  this.lineGraph.config
    //);

    let layout = {
      title: this.childLabel['lineChart'],
      xaxis: {
        title: x_data[0].name,
      },
      yaxis: {
        title: yaxisLabels.join(" & "),
      }
    };

    console.log(layout)

    //var trace1 = {
    //  x: [1, 2, 3, 4],
    //  y: [10, 15, 13, 17],
    //  type: 'scatter'
    //};
    
    //var trace2 = {
    //  x: [1, 2, 3, 4],
    //  y: [16, 5, 11, 9],
    //  type: 'scatter'
    //};
    
    //var trace3 = {
    //  x: [1, 2, 3, 4],
    //  y: [5, 6, 7, 20],
    //  type: 'scatter'
    //};
    
    //var data = [trace1, trace2, trace3];
    
    Plotly.newPlot(this.lineContainer.nativeElement, this.lineGraph, layout);



    

    

  }
  

  drop(event: CdkDragDrop<number[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }


  


}
