import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormService } from 'src/app/services/form.service';


@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  @Input() isDisabled?: boolean;
  @ViewChild("lineGraph", { static: false }) lineGraphContainer!: ElementRef;
  @ViewChild("lineGraphBuilder", { static: false }) lineGraphBuilder!: ElementRef;

  subscription!: Subscription;
  barGraph: any;
  boxPlotGraph: any;
  pieGraph: any; 
  lineGraph: any; 
  message?: any;
  disable? :any;
  labels?: any; //{key: string, value: string}
  data_sum?: number[];
  show:boolean;
  linearNameData: any[];
  x_data: any[] = [];
  y_data: any[] = [];

  
  parentData: any;
  display: boolean;
  displayBox: boolean; 
  displayPie: boolean; 
  displayLinear: boolean; 
  displayLine: boolean; 


  //rideShareData: RideShareData;

  constructor(private service: FormService) {
    this.show = false;  
    this.linearNameData = [];
    this.display = false; 
    this.displayBox = false;
    this.displayPie = false;
    this.displayLinear = false;   
    this.displayLine = false;
    this.labels = []; 
  }

  //this.rideShareData = rideShareData;
  ngOnInit(): void {
    this.subscription = this.service.currentMessage.subscribe(message => this.message = message);
    this.service.currentLabels.subscribe(labels =>  this.labels = labels)
    console.log(this.labels)
    this.parentData = this.message; //pass over the data

    this.display = true;
  }


  displayBarChart() {
    this.display = true;
    this.displayPie = false; 
    this.displayBox = false;
    this.displayLinear = false; 
    this.displayLine = false; 
  }

  displayBoxPlotChart(){
    this.display = false;
    this.displayPie = false; 
    this.displayBox = true;
    this.displayLinear = false; 
    this.displayLine = false; 
  }

  displayPieChart(){
    this.display = false;
    this.displayBox = false;
    this.displayPie = true; 
    this.displayLinear = false; 
    this.displayLine = false; 
  }

  processDataLinearChart(){
    this.display = false;
    this.displayBox = false;
    this.displayPie = false;
    this.displayLinear = true;  
    this.displayLine = false; 
  }

  displayLineChart(){
    this.display = false;
    this.displayBox = false;
    this.displayPie = false; 
    this.displayLinear = false;
    this.displayLine = true;  
  }





}








