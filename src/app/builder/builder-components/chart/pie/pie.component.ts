import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
declare const Plotly:any;


@Component({
  selector: 'app-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.css']
})
export class PieComponent implements OnInit {
  @Input() data: any;
  @Input() childLabel: any;

  
  @ViewChild("pieGraph", { static: false }) pieContainer!: ElementRef;
  labels?: string[];
  data_sum?: number[];
  pieGraph: any; 


  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
  
    const result = this.data.reduce((sums: number, obj: any) => Object.keys(obj).reduce((s: any, k: any) => {    
      k === 'id' || (s[k] = (s[k] || 0) + +obj[k]);
      return s;
      }, sums), {});
      this.labels = Object.keys(result);
      this.data_sum = Object.values(result);
  
  
      this.pieGraph = {
        data : [{
          values: this.data_sum,
          labels: this.labels,
          type: 'pie'
        }], layout : {
          title: this.childLabel['pieChart'],
        }
      };
  
      Plotly.newPlot(
        this.pieContainer.nativeElement,
        this.pieGraph.data,
        this.pieGraph.layout,
        this.pieGraph.config
      );
  
  }

}
