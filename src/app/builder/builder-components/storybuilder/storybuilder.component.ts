import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormService } from '../../../services/form.service';
import '@clr/icons';
import { ClarityIcons, infoStandardIcon} from '@cds/core/icon';
import "@angular/compiler";
import { Location } from '@angular/common';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms'
import { HttpClient } from '@angular/common/http';  
import {GameApiResponse} from '../../../models/scene-api-response';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { timer } from 'rxjs';
import { GameService } from 'src/app/services/game.service';
// import { ConsoleReporter } from 'jasmine';
ClarityIcons.addIcons(infoStandardIcon);

@Component({
  selector: 'app-storybuilder',
  templateUrl: './storybuilder.component.html',
  styleUrls: ['./storybuilder.component.css'],
  
})
export class StoryBuilderComponent implements OnInit, OnDestroy{


	@Input() openIntro: boolean; // come in from storyline --> storybuilder - to check if it comes from storylines editing of story or not
  @Input() disabledStoryTitle?: boolean = false;   
  openModal: boolean;
  openInfoModal: boolean;
  inputSceneNumber: string; 
  addWinLose?: boolean; 
  submitDisabledStatus: boolean; 

  tempStoryStorage: any; //to be use when data is pass from child to parent  

  private _story: GameApiResponse = {
    userID :  0,
    storyId :  "",
    storyTitle : "",
    attributes :  [],
    scenes : [],
  }

  testForm!: FormGroup;
  attributeImages: any; 
  tempImageSet: Set<string>;
  showImages: Set<{}>;
  showImagesArray?: any;
  opened: boolean;
  id: any;
  storylist: any;
  tempStackNo: any;
  openEditLosingScene: boolean;
  openEditWinningScene: boolean;
  inputSceneType: string;
  submitWinDisabledStatus: boolean;
  tempWinLoseStorage: any;
  isLoggedIn?: boolean;
  submitLoseDisabledStatus: boolean;
  openMeterModal: boolean; 
  uploadSuccess: boolean;
  uploadFailed: boolean;
  spinning: boolean;
  disableMeterNextButton: boolean;
  disableSceneNextButton: boolean;
  openWarningModal: boolean = false;
  builderguide: any;
  sequence: number;
  tutorialImage: string;
  meterNameError: boolean;
  disableRemoveMeter: boolean;
  disableAddMeter: boolean;
  allSceneTitle?: string[];
  winningScoreValidate: boolean; 
  allDescription: any;
  allBackGroundImages: any;

  constructor(
    private router: Router, 
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private formService: FormService, 
    private tokenStorageService: TokenStorageService,
    private gameService: GameService

    ) {
      this.tempImageSet = new Set();
      this.showImages = new Set();
      this.showImagesArray = [];
      this.inputSceneNumber = ""; 
      this.openModal = false; 
      this.openInfoModal = false;
      this.submitDisabledStatus = true; 
      this.openEditLosingScene = false; 
      this.openEditWinningScene = false; 
      this.inputSceneType = "";
      this.submitWinDisabledStatus = true; 
      this.submitLoseDisabledStatus = true;
      this.openMeterModal = false; 
      this.uploadSuccess = false; 
      this.uploadFailed = false; 
      this.spinning = false; 
      this.disableMeterNextButton = true;
      this.disableSceneNextButton = true;
      this.openIntro = true; 
      this.opened = false; 
      this.builderguide = [];
      this.sequence =0;
      this.tutorialImage="";
      this.meterNameError = false;
      this.disableRemoveMeter = false;
      this.disableAddMeter = false;
      this.winningScoreValidate = false;
     }

  public get Story(): GameApiResponse{
      return this._story;
    }

  // public AddSceneToLocalStorage(){
  //   const allScenes: GameApiResponse['scenes'] = {
  //     stackNo: this.formService.getScene();
  //     field?: string;
  //     backgroundImage: string;
  //     characterName: string;
  //     characterImage: string;
  //     graphImage: string;
  //     graphData: string;
  //     description: string;
  //     options: Option[];
  //   }
  // }

  openMeterModalFunction(){
    this.openMeterModal = true; 
  }
   
  addStory(){ //dont delete 
    const currentStory:GameApiResponse = {
        userID : 1, // to be added from the userID based on log in user 
        // storyID :  will be define in db,
        storyTitle : this.testForm.value.storyTitle,
        attributes :  [this.attributes.value],
        scenes : [this.scenes.value],
    };
    this.formService.addStory(currentStory);
    // this.formService.updateStory(currentStory['scenes']);
  }

  
  ngOnInit(): void {

    //console.log(this.disabledStoryTitle)

    let result = this.formService.getScene().map( (item: any)=> { return item.sceneTitle }); // get values of the specific keys only 
    this.allSceneTitle = result;

    this.allDescription = this.formService.getScene().map( (item: any)=> { return item.description });
    this.allBackGroundImages = this.formService.getScene().map( (item: any)=> { return item.backgroundImage });
    
    //console.log(result);


    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (!this.isLoggedIn) {
    this.router.navigateByUrl('/login');
    }

    //this.formService.getStories().subscribe((res) => {
    //  this.id = res.userID;
    //  this.storylist = res.stories;
    //})

    this.formService.getAttributeImages().subscribe((res) => {
      //res.images.unshift({"name": "", "url": ""})
      this.attributeImages = res.images;
    })
  
    if (localStorage.getItem('story') === null){

      this.testForm = this.fb.group({
        storyTitle: ['', Validators.required],
        scenes: this.fb.array([]),
        attributes: this.fb.array([])
      })
    } 
    else{
      this.testForm = this.fb.group({
        storyTitle: [this.formService.getStory().storyTitle, Validators.required],
        scenes: this.fb.array([]),
        attributes: this.fb.array([])
    })
    this.patchAttribute();
    }

    
      
      // this.onChanges();

    this.testForm.valueChanges.subscribe((changes: any) => { // to update localstorage on every changes made in story builder 
      this.formService.addStory(changes);
      //console.log(this.testForm.value);
      // this.formService.getScene();

      let result = this.formService.getScene().map( (item: any)=> { return item.sceneTitle }); // get values of the specific keys only 
      this.allSceneTitle = result;

      this.allDescription = this.formService.getScene().map( (item: any)=> { return item.description });
      this.allBackGroundImages = this.formService.getScene().map( (item: any)=> { return item.backgroundImage });

      //console.log(result);

      if (this.testForm.value.attributes.length <1){
        this.disableAddMeter =false;
      } else if(this.disableMeterNextButton==true && this.testForm.value.attributes.length >0){
        this.disableAddMeter = true;
      } else if(this.disableMeterNextButton==false && this.testForm.value.attributes.length >0){
        this.disableAddMeter =false;
      }

    });
    if (this.openModal){ //if modal is open, close the wizard 
      this.opened = false; 
    } 

    this.testForm.get('attributes')?.valueChanges.subscribe((changes: any) => { // to update localstorage on every changes made in story builder 
      //console.log('test att', changes);
      let meternameList:any = [];
      changes.forEach((element:any) => {
        meternameList.push(element.attributeName);
      });
      if (new Set(meternameList).size !== meternameList.length){
        this.meterNameError = true;
        this.disableRemoveMeter = true;
      }else{
        this.meterNameError = false;
        this.disableRemoveMeter = false;
      }
      // if (this.testForm.get("winningscore"))
      let countInValid = 0;
      changes.forEach((element:any) => {
        if (element.winningScore>100 || element.winningScore <0){
          countInValid +=1;
        }
      });
      if (countInValid >0){
        this.winningScoreValidate = true;
      }else{
        this.winningScoreValidate = false;
      }


      // this.formService.updateScenes(changes);
      // console.log(this.testForm.value.attributes);
      this.formService.addAttributes(changes);

      // this.checkAttributes();

    });

    this.testForm.get('storyTitle')?.valueChanges.subscribe((changes: any) => { // to update localstorage on every changes made in story builder 
      this.formService.addStoryTitle(changes);
    });
    // this.testForm.get('scenes')?.valueChanges.subscribe((changes: any) => { // to update localstorage on every changes made in story builder 
    //   this.formService.addScenes(changes);
    // });

    this.testForm.get('attributes')?.statusChanges.subscribe((validToSubmit: string)=>{
      //console.log(validToSubmit)

      if (validToSubmit == "VALID" && this.formService.getAttribute().length > 0 && this.meterNameError == false && this.winningScoreValidate==false){
        this.disableMeterNextButton = false; //we the toggle of disabled button to false  

      } else {
        this.disableMeterNextButton = true; //disable button cannot pass 
      }
    });


    //for the storylines edit function - don delete
    this.opened = !this.openIntro; 

    //check if win lose got scenes from local storage, if have , activate the button to true 
    //addWinLose
    //let winLoseArray = this.formService.getStoredWinLoseScenes();
    //if (winLoseArray.length > 0 ) {
    //  this.addWinLose = true; 
    //}
    

    //!important, checker if the user go and refresh and reloads the components 
    //for scenes 

    if (this.formService.getScene().constructor === Array){

      let storyStatus = this.formService.checkScenesStatus();
      this.disableSceneNextButton = storyStatus;
    } 

    if (this.formService.getAttribute().length > 0) {
      //for meter - check once if refresh
      let attStatus = this.formService.checkAttributeStatus();
      this.disableMeterNextButton = attStatus;
    }



   if (this.formService.getStoredWinLoseScenes().length > 0) { //if not empty
    
    console.log(this.formService.getStoredWinLoseScenes())
    console.log('testsetset')
    this.addWinLose = true

    let winLoseStatus: any  = this.formService.checkWinLoseStatus();
    if (typeof winLoseStatus == "object") {
      console.log(winLoseStatus);

      if (winLoseStatus[0] == true) {
        //this.submitLoseDisabledStatus =  
        this.submitWinDisabledStatus = true;
      } else if (winLoseStatus[1] == false) {
        this.submitWinDisabledStatus = false;
      }
  
      if (winLoseStatus[1] == true) {
        this.submitLoseDisabledStatus = true;  
      } else if (winLoseStatus[1] == false) {
        this.submitLoseDisabledStatus = false;
      }
    } else {

      this.submitWinDisabledStatus = false;
      this.submitLoseDisabledStatus = false;

    }
    

   }


    this.builderguide = [
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/intro-1.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/intro-2.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/story-1.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/story-2.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/story-3.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/meter-1.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/meter-2.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/meter-3.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/meter-4.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/meter-5.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/meter-6.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/scene-1.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/scene-2.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/scene-3.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/scene-4.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/scene-5.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/scene-6.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/scene-7.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/scene-8.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/scene-9.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/scene-10.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/finalscene-1.PNG",
      "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/background/finalscene-2.PNG"
    ]

    this.tutorialImage = this.builderguide[0];

  }

  ngOnDestroy(){
    console.log('component destroyed')
    this.opened = false;
  }

  close(){
    //console.log('close liao')
    this.opened = false; 
    
    let currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }

  toggleInfo(){
    //console.log(this.openInfoModal);
    this.openInfoModal = !this.openInfoModal;
    //console.log(this.openInfoModal);
  }

  tutorialNext(){
    if (this.sequence<this.builderguide.length-1){
      this.sequence +=1;
      this.tutorialImage = this.builderguide[this.sequence];
    }
  }

  tutorialBack(){
    if (this.sequence>0){
      this.sequence -=1;
      this.tutorialImage = this.builderguide[this.sequence];
    }
  }

  resetTutorial(){
    this.sequence =0;
    this.tutorialImage = this.builderguide[this.sequence];
    this.openInfoModal = !this.openInfoModal;
  }

  patchAttribute(){ 
    //console.log('ii');
    //console.log(this.formService.getStory());
		let localStorageAttribute: any = this.formService.getStory().attributes;
    let localStorageScenes: any = this.formService.getScene();
		//console.log(localStorageAttribute); // it contains a array and an array with data 
    let count = 0;
    //console.log("test patching")
    //add attribute 
    localStorageAttribute.forEach((e: any, index: number) => {
      //console.log(index)
      //console.log(e.attributeName)
			let att : FormGroup = this.fb.group({
        attributeName: [e.attributeName, Validators.required],
        winningScore: [e.winningScore, Validators.required],
        attributeDescription: [e.attributeDescription, Validators.required] 
      })

			this.attributes.push(att);
      count++;
		});

    //add scene 
    localStorageScenes.forEach((e: any, index: number) => {
      let sceneform1: FormGroup = this.fb.group({
        stackNo: [index, Validators.required],
        // sceneDetail: this.fb.group({
        sceneTitle: [""],
        sceneType : [""],
        backgroundImage: [""],
        characterImage:  [""], 
        characterName:  [""], 
        description:  [""],
        graphType: [" "],
        graphImage:  [""],
        graphData: [""],
        options: this.fb.array([])
      })
      this.scenes.push(sceneform1);
    });
  }

  get attributes() {
    return this.testForm.get("attributes") as FormArray;
  }

  newMeter(index: number): FormGroup {
      return this.fb.group({
        // meterId: [index, Validators.required],  
        attributeName: ['', Validators.required],
        winningScore: [0, Validators.required], //hardcore for now , rmbr add to html
        attributeDescription: ["", Validators.required] 
      }) 

  }

  // get attributeImage(){
  //   return this.testForm.get("attributeImage");
  // }

  addMeter():void {
    this.attributes.push(this.newMeter(this.attributes.value.length));
    //console.log(this.attributes.value.length);
  }

  removeMeter(i:number) {
		this.attributes.removeAt(i);
	}

  get scenes() {
    return this.testForm.get("scenes") as FormArray
  }

  newScene(index: number): FormGroup {
    return this.fb.group({
      stackNo: [index, Validators.required],
      // sceneDetail: this.fb.group({
      sceneTitle: [""],
      sceneType : "normal",
      backgroundImage: [""],
      characterImage:  [""], 
      characterName:  [""], 
      description:  [""],
      graphType: [" "],
      graphImage:  [""],
      graphData: [""],
      options: this.fb.array([])
      // })
    })
  }

  addScene(): void { // add blank scene template 
    // console.log(this.formService.getScene().value.length)
    // console.log(this.scenes.value.length)
    
    this.scenes.push(this.newScene(this.formService.getScene().length)); //form logic
    // console.log(this.scenes)
    //console.log(this.testForm.value);
    // this.scenes.push(this.newScene(this.scenes.value.length)); //form logic 
    let status = this.formService.addScenes(this.newScene(this.scenes.value.length));

    this.disableSceneNextButton = status;
    //disabled the button till the scene is created and submitted

  }

  addWinningLosingScene(){
    this.addWinLose = true;
  }

  editWinningScene(){
    this.inputSceneType = "winning";
    this.openEditWinningScene = true; 
  }

  editLosingScene(){
    this.inputSceneType = "losing";
    this.openEditLosingScene = true; 
  }

  removeWinningLosingScene(){
    this.addWinLose = false;
    //clear local storage for this segment
    this.formService.removeFinalScene();

    console.log(this.formService.getStoredWinLoseScenes())

    //hardcored 
    this.submitWinDisabledStatus = true;
    this.submitLoseDisabledStatus = true;


  }

  removeScene(i:number) {
		this.scenes.removeAt(i);
    let status = this.formService.removeScene(i);

    this.disableSceneNextButton = status; 

	}

  previewScene(i:number){
    let scenes = this.formService.sendJSONStringForPreview(i)
    this.gameService.previewScene(scenes)
  }

  previewfinalScene(i: string){
    let scenes = this.formService.JSONStringForFinalScenePreview(i)
    this.gameService.previewScene(scenes)
  }

  previewGame(){

    let story  = this.formService.combineJSON();
    console.log(story)
    this.gameService.previewGame(story)
  }

  cancellationWarning(){
    this.openWarningModal = true; 
  }

  clearStorageFromWarningModal(){
    this.formService.removeAllScenes(); // clear scecnes
    this.openWarningModal = false; 
    this.opened = false; //close the wizard 

    //let currentUrl = this.router.url;
    //this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    //this.router.onSameUrlNavigation = 'reload';
    //this.router.navigate([currentUrl]);
    window.location.reload();


  }

  dontClearFromLocalStorage(){
    this.openWarningModal = false;
    this.opened = true;
  }

  //edit the scene form of the particular index - pop up modal of form component
  editSceneForm(sceneNo :number){ 
    this.inputSceneNumber = sceneNo.toString();
    this.openModal = true; 
  }


  beginWizard(){
    this.opened =!this.opened;
  }

  add(): void {
    this.router.navigateByUrl('/create');
  }

  btnClick(){
   this.router.navigateByUrl("/create");
  }

  toggleModal($event: boolean){
     console.log($event);
    this.submitDisabledStatus = $event; 
  }

  toggleWinModal($event: boolean){
    this.submitWinDisabledStatus = $event;
  }

  toggleLoseModal($event :boolean){
    this.submitLoseDisabledStatus = $event;
  }

  receiveSceneFrmChild($event: any){ //on going function
    this.tempStoryStorage = $event;
  }

  receiveSceneTypeFrmChild($event: any){
    this.tempWinLoseStorage = $event;
  }



  receiveStackNoFrmChild($event: any){
    //console.log($event)
    this.tempStackNo = $event;
  }

  //submit / update scene to localstorage 
  submitSceneOnClick(){
    //add story to the local Storage 
    // this.formService.addStory(this.tempStoryStorage);
    // console.log(this.tempStoryStorage, this.tempStoryStorage.stackNo);
    //console.log(this.tempStoryStorage);
    if (this.tempStoryStorage.characterImage == " "){
      this.tempStoryStorage.characterImage = "";
    }
    //console.log(this.tempStoryStorage);
    let status = this.formService.addSceneContent(this.tempStoryStorage, this.tempStackNo);

    console.log(status);

    this.disableSceneNextButton = status;


    //console.log(this.tempStackNo);

    //this.formService.combineJSON();

    this.openModal = false;//close the modal 

    let result = this.formService.getScene().map( (item: any)=> { return item.sceneTitle }); // get values of the specific keys only 

    this.allDescription = this.formService.getScene().map( (item: any)=> { return item.description });
    this.allBackGroundImages = this.formService.getScene().map( (item: any)=> { return item.backgroundImage });


    this.allSceneTitle = result;


    console.log(this.formService.getScene());

    
  }

  submitWinOnClick(){
    let storedWinLose = this.formService.getStoredWinLoseScenes();
    //console.log(this.tempWinLoseStorage)
    let index = storedWinLose.findIndex((x: any) => x.sceneType =="winning");
    //console.log(index)
    if (index > -1){
      this.formService.updateFinalScene(this.tempWinLoseStorage, index);
    }else{
      this.formService.addFinalScene(this.tempWinLoseStorage);
    }
    this.openEditWinningScene = false;//close the modal 


    //console.log(this.formService.getStoredWinLoseScenes());
  }

  submitLostOnClick(){
    let storedWinLose = this.formService.getStoredWinLoseScenes();
    //console.log(this.tempWinLoseStorage)
    let index = storedWinLose.findIndex((x: any) => x.sceneType =="losing");
    //console.log(index)
    if (index > -1){
      this.formService.updateFinalScene(this.tempWinLoseStorage, index);

    }else{
      this.formService.addFinalScene(this.tempWinLoseStorage);
    }
    this.openEditLosingScene = false;//close the modal
    
    //console.log(this.formService.getStoredWinLoseScenes());
    
  }


  //for testing
  clearLocalStorage(){
    this.formService.removeAllScenes();

    //after clear local storage, need to refresh the page to show that it have cleared
    let currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);

  }

  submitStory(){ //submit everything under the sun 
    // clear local storage after it is sent successfully
  
    let value = this.formService.combineJSON();

     
      //console.log('testset')


      this.spinning = true; //start spinning irregardless  

      this.formService.submitStory(value).subscribe(
        
        {
          next: res => {

        if (res.status === 200){

          //console.log('200 ok')
  
          timer(2000).subscribe(
            val => {
              this.spinning = false;
              this.uploadSuccess = true
  
            timer(2000).subscribe(
            val => {
            this.uploadSuccess = false;
  
            this.formService.removeAllScenes();
  
            let currentUrl = this.router.url;
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.onSameUrlNavigation = 'reload';
            this.router.navigate([currentUrl]);
  
            })})
          }
          }, 

          error: error => {

          
          //console.log('not okay ')
  
          timer(2000).subscribe(
            val => {
              this.spinning = false;
              this.uploadFailed = true;
  
          timer(10000).subscribe(
            val => {
              this.uploadFailed = false
            })})


          }
      });
  }




}
