import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;
  isLoggedIn: boolean;
  roles: string[] = [];
  isLoginFailed: boolean;
  errorMessage: any;

  constructor(private router: Router, private fb: FormBuilder, private authService: AuthService, private tokenStorage: TokenStorageService) {
    this.isLoginFailed = false; 
    this.isLoggedIn = false; 
  }

  ngOnInit(): void {

    this.loginForm = this.fb.group(
      {
        username: ['', Validators.required],
        password: ['', Validators.required],
      }
    )

    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
  }

  onSubmitLogin(){
    //console.log(this.loginForm.value);
    //this.router.navigateByUrl("");
    const { username, password } = this.loginForm.value;

    this.authService.login(username, password).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        // this.reloadPage();
        this.router.navigateByUrl('/stories');
      },
      err => {
        this.errorMessage = "Login Failed";
        //console.log(this.errorMessage);
        this.isLoginFailed = true;
      }
    );
  }

  onRegister(){
    this.router.navigateByUrl("registration");
  }
  
  onForgotPassword(){
    this.router.navigateByUrl("forgetpassword");
  }

  reloadPage(): void {
    window.location.reload();
  }


}
