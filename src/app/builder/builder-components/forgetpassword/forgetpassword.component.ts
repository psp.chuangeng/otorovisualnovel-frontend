import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {

  forgetPasswordForm!: FormGroup;
  
  constructor(private router: Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.forgetPasswordForm = this.fb.group(
      {
        email: ['', [Validators.required, Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      }
    )
  }

  onSubmitEmail(){
    console.log(this.forgetPasswordForm.value);
    this.router.navigateByUrl("checkemail");
  }

  backToLogin(){
    this.router.navigateByUrl("login");
  }

  get email() {
    return this.forgetPasswordForm.get('email');
  }





}
