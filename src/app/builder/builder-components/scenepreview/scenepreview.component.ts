import { ChangeDetectorRef, Component, OnInit, ChangeDetectionStrategy, OnDestroy, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormService } from 'src/app/services/form.service';
import { GameService } from 'src/app/services/game.service';
import * as XLSX from "xlsx";
// import { ChartComponent } from '../../../builder/builder-components/chart/chart.component';
// import {NgZone} from '@angular/core';
import { timer } from 'rxjs';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  AnimationEvent,
  query, 
  stagger,
  sequence
} from '@angular/animations';

//animation for fade in and fade out effect + / - for feedback effect on meter  
const enterTransition = transition(':enter', [
  style({
    opacity: 0
  }),
  animate('1s ease-in', style({
    opacity: 1
  }))
]);

const leaveTrans = transition(':leave', [
  style({
    opacity: 1
  }),
  animate('1s ease-out', style({
    opacity: 0
  }))
])

const fadeIn = trigger('fadeIn', [
  enterTransition
]);

const fadeOut = trigger('fadeOut', [
  leaveTrans
]);
@Component({
  selector: 'app-scenepreview',
  templateUrl: './scenepreview.component.html',
  styleUrls: ['../../../game/game-components/scene/scene.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [fadeIn, fadeOut],
})
@HostListener('window:beforeunload', ['$event'])
export class ScenepreviewComponent implements OnInit, OnDestroy{
  

  constructor(public dialog: MatDialog, private router: Router, private service : GameService, private form: FormService, private cf: ChangeDetectorRef) { 
    this.openChartModal = false;
    this.attributeShowEffect = [];
    this.showDamageEffect = false; 
    this.message = localStorage.getItem('response')
  }
  showDamageEffect: boolean;  
  attributeShowEffect: any[];
  sequence : number = 0;
  playerName : string = "";
  // gameCode : string = "";
  sceneLength = 0;
  attributeName : any = [];
  attributeDict : {[key: string]:number} = {};
  attributeDescDict : {[key: string]:string} = {};
  winCondition: any = {};
  scenes : any;
  currentScene : any = {};
  impact : string = "";
  impactNext: any = false;
  // winLose: any;
  // endOfGame : boolean = false;
  // startOfGame : boolean = false;
  // resultAttribute : any = {};
  // resultAttributeList : any = [];
  // resultPost: any;
  // clickAudio : any;
  // bgAudio : any;
  // winAudio : any;
  // loseAudio : any;

  open = true;
  // size = "xl"
  // tutorialImg : any;
  // tutorialButtonText: string = "Next";
  
  animDone: boolean = false;
  // SCENE
  background : string = "";
  optionLength : number = 0;
  
  // ANIMATIONS 
  cState: string = 'enter1';
  textWrapper: any = "Hello";
  textWrapper2: any = "Hello";
  text: any = {};
  impactText: any = {};
  textDisplayed: string = "";
  //disableBarChart: boolean;
  openChartModal: boolean;
  message: any = "default";

  
ngOnDestroy(){

}




 ngOnInit(): void {
    this.impactNext = false;
    this.service.loadFromLocal(JSON.parse(this.message))
    // this.service.currentSceneList.subscribe(message => this.message = message)
    this.retrieveGame();

  }

  retrieveGame(): void {
    this.service.playerName.subscribe((data: string) => this.playerName = data)
    var storyDeets = this.service.storyDetails
    for (let key in storyDeets){
      this.attributeDict[storyDeets[key].attributeName] = 100;
      this.attributeDescDict[storyDeets[key].attributeName] = storyDeets[key].attributeDescription;
      this.winCondition[storyDeets[key].attributeName] = storyDeets[key].winningScore; 
      
    }
    // this.sequence = 1
    this.loadScene()
    ;
  }
  closeShowDamageEffect(){
    this.showDamageEffect = false
    this.attributeShowEffect = []
  }

  optionClick(selection: any){
    console.log(selection);

    if (selection.impactScene != "") {
      this.currentScene.graphData = ""

      this.impactNext = true
      this.impact = selection.impactScene
      
      selection.consequences.forEach( (consequence: any) =>{

        console.log(consequence.attributeName, consequence.effect)
        this.attributeShowEffect.push({showName: consequence.attributeName, showEffect: consequence.effect})
        console.log(typeof(consequence.effect))
        console.log(consequence.effect)
        this.attributeDict[consequence.attributeName] += Number(consequence.effect);
      });
      // setTimeout(() => this.ngAfterViewInit());
      console.log(this.sequence)

      this.showDamageEffect = true
      console.log(this.attributeShowEffect)

      timer(2000).subscribe(
        val => {
          this.closeShowDamageEffect();
        }
      )

      
    }
    else{
      console.log(this.sequence)
      selection.consequences.forEach( (consequence: any) =>{

        console.log(consequence.attributeName, consequence.effect)
        this.attributeShowEffect.push({showName: consequence.attributeName, showEffect: consequence.effect})

      this.attributeDict[consequence.attributeName] += Number(consequence.effect);
      console.log(typeof(consequence.effect))
      console.log(consequence.effect)
      });
      this.sequence += 1;
      this.loadScene()
      // setTimeout(() => this.ngAfterViewInit());

      this.showDamageEffect = true
      console.log(this.attributeShowEffect)

      timer(2000).subscribe(
        val => {
          this.closeShowDamageEffect();
        }
      )

    }
    
    
  }

  loadScene(){
    this.currentScene = this.service.getPreviewScene()
    this.textDisplayed = this.currentScene.description
    this.optionLength = this.currentScene.options.length
    this.background = this.currentScene.backgroundImage
    // this.impactNext = false;
  
    // if(this.open == false){
    //     setTimeout(() => this.ngAfterViewInit());
    // }
    // }
    
  }
  // HEALTH BAR MECHANICS

  attributeColor(n:any){
    if (n == 1)
    {
      return "#FF0000";
      
    }
    else if (n == 2)
    {
      return "#c35817";
    }
    else (n == 3)
    {
      return "#96CC7F";
    }
  }

  callExcel(link: string){
    this.excelUrl(link); 
  }

  openChart(link : string) {
    console.log(link);
    // this.excelUrl(link);
		//const dialogRef = this.dialog.open(ChartComponent);
		 this.openChartModal = true;
  

	}

    //excelUrl
  excelUrl(link : string){ //activate the charts here once excel link is drawn in
    console.log(link)
    if (link != "") {
      this.form.getExcelDataFromUrl(link).subscribe((res: any) => {
			
        var data = new Uint8Array(res);
        var arr = new Array();
        for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");
          /* Call XLSX */
          var workbook = XLSX.read(bstr, {type:"binary"});
  
  
          /* DO SOMETHING WITH workbook HERE */
          var first_sheet_name = workbook.SheetNames[0];
          /* Get worksheet */
          var worksheet = workbook.Sheets[first_sheet_name];
          
          console.log(worksheet)
  
  
          let graphLabel =  {'barChart': worksheet.A3 == undefined ? "" : worksheet.A3.v, 'lineChart': worksheet.A5 == undefined ? "" : worksheet.A5.v , 'pieChart': worksheet.A7 == undefined ? "" : worksheet.A7.v, 'boxPlot': worksheet.A9 == undefined ? "" : worksheet.A9.v};
  
          
          worksheet['!ref'] = "C1:F151";
  
          
          let x: any[] = XLSX.utils.sheet_to_json(worksheet,{raw:true});
  
  
          
  
          //data manipulation again to serve charts 
          const keys = Object.keys(x[0]);
          //console.log(keys);
          // this.displayedColumns = keys; 
          // this.dataSource = x;
          //this.postsForm.get('excel')?.setValue(x);
  
          //console.log(x)
          this.form.changeMessage(x, graphLabel); // pass data to chart 
          //this.disableBarChart = false;
  
          //this.openChart(link);
          this.openChartModal = true;


    })
  }
    
  }

  }