import { Component, OnInit } from '@angular/core';
import '@cds/core/icon/register.js';
import { ClarityIcons, userIcon, homeIcon, uploadIcon, listIcon, logoutIcon } from '@cds/core/icon';
import { TokenStorageService } from 'src/app/services/token-storage.service';

ClarityIcons.addIcons(userIcon);
ClarityIcons.addIcons(homeIcon);
ClarityIcons.addIcons(uploadIcon);
ClarityIcons.addIcons(listIcon);
ClarityIcons.addIcons(logoutIcon);

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedIn = false;
  currentUser: any;

  constructor(private tokenStorageService: TokenStorageService, ) { }

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }

}
