import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WinlosesceneComponent } from './winlosescene.component';

describe('WinlosesceneComponent', () => {
  let component: WinlosesceneComponent;
  let fixture: ComponentFixture<WinlosesceneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WinlosesceneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WinlosesceneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
