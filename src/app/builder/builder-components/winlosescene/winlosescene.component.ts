import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormService } from 'src/app/services/form.service';
import { ImageUploadService } from 'src/app/services/image-upload.service';

@Component({
  selector: 'app-winlosescene',
  templateUrl: './winlosescene.component.html',
  styleUrls: ['./winlosescene.component.css']
})
export class WinlosesceneComponent implements OnInit {

	winLoseForm!: FormGroup;
  backgroundImages: any[];
  characterImages: any[];

	@Input() outgoingSceneType: string; // come in from storyBuilder --> sceneBuilder 
	@Output() sendSceneTypeToParent = new EventEmitter<any>(); // sceneBuilder --> storyBuilder 
	@Output() toggleWinModal = new EventEmitter<boolean>(); // for winning scene  sceneBuilder --> storyBuilder   
  @Output() toggleLoseModal = new EventEmitter<boolean>(); //for losing scene sceneBuilder --> storyBuilder 

  constructor(private fb: FormBuilder, private formService: FormService, private imageService: ImageUploadService) { 
		this.backgroundImages = [];
		this.characterImages = [];
    this.outgoingSceneType = "";
  }

  ngOnInit(): void {

    // console.log(this.outgoingSceneType)
    // console.log(this.formService.getStoredWinLoseScenes())
    if (this.formService.getStoredWinLoseScenes()==[]){
      // console.log("hi")
      this.winLoseForm = this.fb.group(
        {
          sceneType: [this.outgoingSceneType],
          backgroundImage: ['', Validators.required],
          characterImage:  [''], //fixed for now 
          characterName:  [''], 
          description:  ['', Validators.required],
          graphImage:  "",
          graphData: "",
          options: [],
        }
      )
    } 
    else {
      let storedWinLose = this.formService.getStoredWinLoseScenes();
      let index = storedWinLose.findIndex((x: any) => x.sceneType ==this.outgoingSceneType);
      // console.log(index)
      //console.log('re init')
      if (index >= 0){
        this.winLoseForm = this.fb.group(
          {
            sceneType: [this.outgoingSceneType],
            backgroundImage: [storedWinLose[index].backgroundImage, Validators.required],
            characterImage:  [storedWinLose[index].characterImage], //fixed for now 
            characterName:  [storedWinLose[index].characterName], 
            description:  [storedWinLose[index].description, Validators.required],
            graphImage:  "",
            graphData: "",
            graphType: "",
            options: [[]],
            sceneTitle: ""
          }
        )
      }else{

        //console.log('haven')

        this.winLoseForm = this.fb.group(
          {
            sceneType: [this.outgoingSceneType],
            backgroundImage: ['', Validators.required],
            characterImage:  [''], //fixed for now 
            characterName:  [''], 
            description:  ['', Validators.required],
            graphImage:  "",
            graphData: "",
            graphType: "",
            options: [[]],
            sceneTitle: ""
          }
        )
      }
      
    }
    
    this.imageService.getImagesFromDB().subscribe((res: any) => {
			//console.log(JSON.parse(res.body))
			let datas = JSON.parse(res.body);
      this.characterImages = [{"name":"none", "url":" "}];
      
			datas.forEach((data: any) => {
        let output = data.imageUrl.split("/");
				let imageName = output[output.length - 1];

				if (data.imageType == "character") {
					this.characterImages.push({"name":imageName, "url":data.imageUrl})
				} else if (data.imageType == "background") {
					this.backgroundImages.push({"name":imageName, "url":data.imageUrl})
				}
			})
		})

    // console.log(this.winLoseForm.value)

    //this.formService.getBackgroundImages().subscribe((res) => {
		//	// this.backgroundImages = [{"name":"none", "url":" "}];
		//	// res.images.forEach(element => {
		//	// 	this.backgroundImages.push(element);
		//	// });
		//	this.backgroundImages = res.images;
		//	// this.backgroundImages.unshift({"name":"none", "url":" "});
		//	// console.log(this.backgroundImages);
		//})

    //this.formService.getCharacterImages().subscribe((res) => {
		//	this.characterImages = [{"name":"none", "url":" "}];
		//	res.images.forEach(element => {
		//		this.characterImages.push(element);
		//	});
		//	// this.characterImages = res.images;
		//})

    		//check for ongoing chanegs and send the data and status of a valid response to the LandingpageComponent - submit button
        
    //console.log(this.outgoingSceneType)

    if (this.formService.getStoredWinLoseScenes() != []) {
      this.sendSceneTypeToParent.emit(this.winLoseForm.value);
    } else {
      this.winLoseForm.value.sceneType == "winning" ? this.toggleWinModal.emit(true): this.toggleLoseModal.emit(true) 
    }

		this.winLoseForm.statusChanges.subscribe((validToSubmit: string)=>{

			if (validToSubmit == "VALID"){

        this.winLoseForm.value.sceneType == "winning" ? this.toggleWinModal.emit(false): this.toggleLoseModal.emit(false) //change status of button  

				//send the winning / losing scene over to the main page to submit 
				// let sceneNumber: any = this.stackNo;
				// let scenes: any =  this.service.getScene(); //scene from localstorage
				
				let outgoingData = Object.assign(this.winLoseForm.value); //to update the scene of the particular index  
				
				// this.service.getStory().scenes[sceneNumber] = story.scenes[sceneNumber];
				// console.log(story.scenes[sceneNumber]);
				// console.log(this.service.getStory());
				// console.log(scenes[sceneNumber]);
        //console.log(outgoingData, "YAY")

				this.sendSceneTypeToParent.emit(outgoingData);
				// this.sendStackNo.emit(this.stackNo); 

			} else {
        //console.log('no')
        this.winLoseForm.value.sceneType == "winning" ? this.toggleWinModal.emit(true): this.toggleLoseModal.emit(true) 
			}
		})


    
  }

}
