import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  registrationForm!:FormGroup;
  notSame: boolean;  

  isSuccessful: boolean;
  isSignUpFailed: boolean;
  errorMessage: string; 
  spinning: boolean = false;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) { 
    this.notSame = false;
    this.isSignUpFailed = false; 
    this.isSuccessful = false; 
    this.errorMessage = '';
  }

  ngOnInit(): void {


    //this.errorMessage = "Registration Failed";
    //this.isSignUpFailed = true;
    //this.isSuccessful = true; 
    
    this.registrationForm = this.fb.group(
      {
        username: ['', Validators.required],
        email: ['', [Validators.required, Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
        password: ['', [Validators.required, Validators.pattern('(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,15}') ] ],
        confirmPassword: ['', [Validators.required,  Validators.pattern('(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,15}') ]],
      }
      //(?=\\D*\\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,15}') --> strict password regex 
      /**  
       *     <ul> <strong>Password to be  :</strong>
        <li>At least 8 - 15 characters in length</li>
            <li>at least one Lowercase letters</li>
            <li>at least one Uppercase letters</li>
            <li>at least one Digit</li>
            <li>at least 1 special character</li>
    </ul>
       * 
      */
    )

  }

  get email() {
    return this.registrationForm.get('email');
  }

  get password() {
    return this.registrationForm.get('password');
  }

  
  get confirmPassword() {
    return this.registrationForm.get('confirmPassword');
  }

  onLogin(){
    this.router.navigateByUrl("login");
  }

  onSubmitNewAccount(){

    this.spinning = true; 
    const { username, email, password, confirmPassword } = this.registrationForm.value;

    if (password == confirmPassword) {
      this.authService.register(username, email, password).subscribe(
        data => {
          console.log(data);
          this.isSuccessful = true;
          //this.isSignUpFailed = false;

          //route to home page ? 
          timer(2000).subscribe(
            val => {
              this.spinning = false;
              this.isSuccessful = true;

              timer(2000).subscribe(
                val => {
                  this.isSuccessful = false
                  //route to login 
                  this.router.navigateByUrl('/login');
                }
              )
            }
          )
          
        },
        err => {

          timer(2000).subscribe(
            val => {
              this.spinning = false; 
              this.errorMessage = "Registration Failed";
              console.log(this.errorMessage)
              this.isSignUpFailed = true;
            }
          )
        }
      );

      console.log(this.registrationForm.value);

    } else {

      this.notSame = true; 

    }
  }

  route(){
    this.router.navigateByUrl('/login');
  }


}
