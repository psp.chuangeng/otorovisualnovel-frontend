import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkresetemail',
  templateUrl: './checkresetemail.component.html',
  styleUrls: ['./checkresetemail.component.css']
})
export class CheckresetemailComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }


  backToLogin(){
    this.router.navigateByUrl("login");
  }


}
