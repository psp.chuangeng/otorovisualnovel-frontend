import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckresetemailComponent } from './checkresetemail.component';

describe('CheckresetemailComponent', () => {
  let component: CheckresetemailComponent;
  let fixture: ComponentFixture<CheckresetemailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckresetemailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckresetemailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
