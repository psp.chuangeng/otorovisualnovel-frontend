import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HostgameComponent } from './hostgame.component';

describe('HostgameComponent', () => {
  let component: HostgameComponent;
  let fixture: ComponentFixture<HostgameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HostgameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HostgameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
