import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { StoriesService } from 'src/app/services/stories.service';


@Component({
  selector: 'app-hostgame',
  templateUrl: './hostgame.component.html',
  styleUrls: ['./hostgame.component.css']
})
export class HostgameComponent implements OnInit {

  isDisabled: boolean;
  hostForm!: FormGroup;

  @Input() childMessage?: any;
  results: any;
  gameCode: string;
  openResultModal: boolean;
  constructor(private fb: FormBuilder, private storyService: StoriesService) {
      this.isDisabled = false; 

      this.gameCode = "";
      this.openResultModal = false;
    }

  ngOnInit(): void {
    console.log(this.childMessage);
    this.hostForm = this.fb.group(
      {
      className : ['', Validators.required]
      }
    )
  }

  startGame(){

    console.log(this.className!.value);
    this.storyService.getCodeFromDB(this.className!.value, this.childMessage.storyTitle).subscribe((res: any) => { 
      if (res.status == 200){
        this.gameCode = res.body
        console.log(this.gameCode)
        this.storyService.gameResult(this.gameCode).subscribe((res:any) => {
          if (res.status == 200){
            this.results = JSON.parse(res.body)
            console.log(this.results)
            this.openResultModal = true
          }
        });

      }
    });

    //post request to backend for the className

    //retrieve the gameCode upon response 

    //route and open a 'live' result page of listener with the gameCode, can see how many ppl join the game and finish

  }

  get className(){
		return this.hostForm.get('className');
	}

}
