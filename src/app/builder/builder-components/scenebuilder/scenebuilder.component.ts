import { Component, OnInit, Input} from '@angular/core';
//import { HttpClient, HttpParams } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FormArray } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormService } from '../../../services/form.service';
import { Subscription } from 'rxjs';
import * as XLSX from "xlsx";
import {GameApiResponse} from '../../../models/scene-api-response';
import { Output, EventEmitter } from '@angular/core';
import { ImageUploadService } from 'src/app/services/image-upload.service';


@Component({
	selector: 'app-scenebuilder',
	templateUrl: './scenebuilder.component.html',
	styleUrls: ['./scenebuilder.component.css'],
})
export class SceneBuilderComponent implements OnInit{

	@Input() incomingSceneNumber: string; // come in from storyBuilder --> sceneBuilder 

	toggleFormDialog: boolean; 

	@Output() toggleModal = new EventEmitter<boolean>(); // sceneBuilder --> storyBuilder  

	@Output() sendStoryToParent = new EventEmitter<any>(); // sceneBuilder --> storyBuilder 
	
	@Output() sendStackNo = new EventEmitter<any>(); // sceneBuilder --> storyBuilder 
	
	private _story: GameApiResponse = {
		userID :  0,
		storyId :  "",
		storyTitle : "",
		attributes :  [],
		scenes : [],
	  }

	subscription!: Subscription;
	postsForm!: FormGroup;
	testMeterAttr!: FormGroup;
	decisionGroup!: FormGroup;
	apiResponse: any;
	dataSource: any;
	displayedColumns?: string[];
	backgroundImages: any;
	graphDataList: any;
	graphImages: any; 
	characterImages: any; 
	disableBarChart!: boolean;
	stackNo!: number;
	backgroundName?: string;
	meterList: any; 
	selectedOptionList: any = [];
	rangeValue?: number;
	sceneTypes: string[]; 
	attributeImage: string[]; 
	openChartModal: boolean;
	openGraphImageDropDown: boolean;
	openGraphDataDropDown: boolean;
	meterEffectValidate: boolean;

	constructor(private router: Router, private fb: FormBuilder, private route: ActivatedRoute, private service: FormService, private imageService: ImageUploadService) {
		this.disableBarChart = true; 
		this.apiResponse = [];
		this.backgroundImages = [];
		this.graphImages = [];
		this.graphDataList = [];
		this.characterImages = [];
		this.meterList = [];
		this.incomingSceneNumber = "";
		this.toggleFormDialog = true;
		this.sceneTypes = ["Normal", "Winning", "Losing"]
		this.attributeImage = [];
		this.openChartModal = false;
		this.openGraphImageDropDown = false; 
		this.openGraphDataDropDown = false; 
		this.meterEffectValidate = false;
	}

	public get Story(): GameApiResponse{
		return this._story;
	  }

	ngOnInit(): void {

		//console.log(this.incomingSceneNumber);
		//this.stackNo = this.route.snapshot.paramMap.get('id')!; // scene id that taken from routeed id
		//console.log(this.incomingSceneNumber);
		this.stackNo = parseInt(this.incomingSceneNumber); //.toString()  
			var storyScene: any = this.service.getScene();

			this.postsForm = this.fb.group(
			
				{
					sceneType : [storyScene[this.stackNo].sceneType ],
					sceneTitle: [storyScene[this.stackNo].sceneTitle, Validators.required],
					backgroundImage: [storyScene[this.stackNo].backgroundImage, Validators.required],
					characterImage:  [storyScene[this.stackNo].characterImage], //fixed for now 
					characterName:  [storyScene[this.stackNo].characterName], 
					description:  [storyScene[this.stackNo].description, Validators.required],
					graphType:  [storyScene[this.stackNo].graphType],
					graphImage:  [storyScene[this.stackNo].graphImage],
					graphData: [storyScene[this.stackNo].graphData],
					options: this.fb.array([]),
					//excel: ['']
				}
			); 
			this.patchOptions();


		//images

		///////////////////////////////////////////

		this.imageService.getImagesFromDB().subscribe((res: any) => {
			//console.log(JSON.parse(res.body))
			let datas = JSON.parse(res.body);

			this.characterImages = [{"name":"none", "url":" "}];
			datas.forEach((data: any) => {
				let output = data.imageUrl.split("/");
				let imageName = output[output.length - 1];

				if (data.imageType == "character") {
					this.characterImages.push({"name": imageName, "url":data.imageUrl})
				} else if (data.imageType == "graph") {
					this.graphImages.push({"name": imageName, "url":data.imageUrl})
				} else if (data.imageType == "graphData") {
					this.graphDataList.push({"name": imageName, "url":data.imageUrl})
				} else if (data.imageType == "background") {
					this.backgroundImages.push({"name": imageName, "url":data.imageUrl})
				}
			})
		})


		//this.service.getBackgroundImages().subscribe((res) => {
		//	this.backgroundImages = res.images;
		//})

		//this.service.getGraphImages().subscribe((res) => {
		//	this.graphImages = res.images;
		//})

		//this.service.getCharacterImages().subscribe((res) => {
		//	this.characterImages = [{"name":"none", "url":" "}];
		//	res.images.forEach(element => {
		//		this.characterImages.push(element);
		//	});
		//})

		//this.service.getGraphData().subscribe((res) => {
		//	this.graphDataList = res.images;
		//})

		//////////////////////////////////////


		this.meterList = this.service.getStory().attributes; //[0]

		//check for ongoing changes and send the data and status of a valid response to the LandingpageComponent - submit button 
		this.postsForm.statusChanges.subscribe((validToSubmit: string)=>{
			//console.log(validToSubmit)
			//console.log(this.validatorForGraph())

			if (validToSubmit == "VALID" && this.validatorForGraph()==true && this.meterEffectValidate==false){
				this.toggleModal.emit(false); //change the toggle of the modal 

				//send the story over to the main page to submit 
				let sceneNumber: any = this.stackNo;
				let scenes: any =  this.service.getScene(); //scene from localstorage
				
				scenes[sceneNumber] = Object.assign(scenes[sceneNumber], this.postsForm.value); //to update the scene of the particular index  

				this.sendStoryToParent.emit(scenes[sceneNumber]);
				this.sendStackNo.emit(this.stackNo); 

			} else {
				this.toggleModal.emit(true);
			}
		})

		//show first render - revistit --> show
		this.callGraph(this.graphData?.value)

		this.graphData?.valueChanges.subscribe((value : string) => { //open interactive chart if url is not empty 
				//console.log(value)
				this.callGraph(value);
				
		})


		//when first render - revistit --> show 
		this.graphTypechecker(this.graphType?.value);
		
		//on gg changes of graph type 
		this.graphType?.valueChanges.subscribe((type: string) => {
			//console.log(type)
			this.graphTypechecker(type);
			
		})
		
		this.postsForm.get('options')?.valueChanges.subscribe((changes:any) => {
			let count = 0;
			//console.log(changes)
			changes.forEach((element:any) => {
				element.consequences.forEach((conseq: any) => {
					if (conseq.effect<-100 || conseq.effect>100){
						count +=1;
					}
				});
				//console.log("ello", changes)
			});
			if (count>0){
				this.meterEffectValidate = true;
				// this.toggleModal.emit(false);
			}else{
				this.meterEffectValidate = false;
				// this.toggleModal.emit(true);
			}
			// console.log(count)
		});

	}


	//////////////////////////functions here/////////////////////

	validatorForGraph(){
		//check if have 
		if (this.graphType?.value != " "){
			if (this.graphType?.value == "Data" && this.graphData?.value) {
				return true
			} 

			if (this.graphType?.value == "Image" && this.graphImage?.value) {
				return true 
			} 

			return false;

		} else {
			//console.log('none')
			//that means person nvr enter anything --> this is None
			return true
		}
		
	}
	 
	graphTypechecker(type: string){
		if (type == "Image") {
			this.openGraphImageDropDown = true;
			this.openGraphDataDropDown = false;
			 this.postsForm.controls['graphData'].setValue('');
		} else if (type == "Data") {
			this.openGraphImageDropDown = false;
			this.openGraphDataDropDown = true;
			 this.postsForm.controls['graphImage'].setValue('');
			
		} else {
			this.openGraphImageDropDown = false;
			this.openGraphDataDropDown = false;
			 this.postsForm.controls['graphData'].setValue('');
			 this.postsForm.controls['graphImage'].setValue('');
		}
	}

	callGraph(value:string){


		if (value != ""){
			//https://d2cb9spvw8xnlm.cloudfront.net/graphData/iris.xlsx

			//http://otoro-image-gallery.s3.amazonaws.com/graphData/iris.xlsx
			console.log(value)
		this.service.getExcelDataFromUrl(value).subscribe((res: any) => {

			console.log(res)
			var data = new Uint8Array(res);
			var arr = new Array();
			for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
			var bstr = arr.join("");
				/* Call XLSX */
				var workbook = XLSX.read(bstr, {type:"binary"});

				/* DO SOMETHING WITH workbook HERE */
				var first_sheet_name = workbook.SheetNames[0];
				/* Get worksheet */
				var worksheet = workbook.Sheets[first_sheet_name];

				let graphLabel =  {'barChart': worksheet.A3 == undefined ? "" : worksheet.A3.v, 'lineChart': worksheet.A5 == undefined ? "" : worksheet.A5.v , 'pieChart': worksheet.A7 == undefined ? "" : worksheet.A7.v, 'boxPlot': worksheet.A9 == undefined ? "" : worksheet.A9.v};

				worksheet['!ref'] = "C1:F151";


				let x: any[] = XLSX.utils.sheet_to_json(worksheet,{raw:true});

				//data manipulation again to serve charts 
				const keys = Object.keys(x[0]);
				//console.log(keys);
				this.displayedColumns = keys; 
				this.dataSource = x;
				//this.postsForm.get('excel')?.setValue(x);
				this.service.changeMessage(x, graphLabel); // pass data to chart 
				this.disableBarChart = false;

		})
		
	} else {
		this.disableBarChart = true;
		}
	}

	openChart() {
			this.openChartModal = true;
	}


	newDecision() {
		this.decisionGroup =  this.fb.group({
			optionName: ['', Validators.required], //decision
			impactScene: ['', Validators.required],
			consequences: this.fb.array([])
			// attributes: this.fb.array([]),
			//hidden form - attribute name 
			//spinner for number  
			// drop down list for + / - 
		});

		this.meterList.forEach((element: any, index: number) => {

			//to display the image from the attribute list

			this.testMeterAttr = this.fb.group({
				attributeName: [element.attributeName, Validators.required],
				effect: [0, Validators.required] //, Validators.min(-100), Validators.max(100)]] //  Validators.minLength(1), Validators.min(0), Validators.max(100)] 
			});
			(this.decisionGroup.get("consequences") as FormArray).push(this.testMeterAttr);
		});

		return this.decisionGroup;
		
	}

	consequences(i: number): FormArray{
		return this.options.at(i).get("consequences") as FormArray;
	}

	addDecision(): void {
		this.options.push(this.newDecision());
	}

	removeDecision(i:number) {
		this.options.removeAt(i);
	}


	patchOptions(){ // to populate attribute list
		//console.log(this.service.getScene())  
		
		var storyScene: any = this.service.getScene();
		// console.log(storyScene);
		var data3 = storyScene[this.stackNo].options;

		// var data3 = storyScene[this.stackNo].options;

		// console.log(data3);
    	data3.forEach((e: any) => {
			var op : FormGroup = this.fb.group({
				optionName: [e.optionName, Validators.required], //decision
				impactScene: [e.impactScene, Validators.required],
				consequences: this.fb.array([]),
			});
			this.options.push(op);
			e.consequences.forEach((element2: any) => {
				
				var attr: FormGroup = this.fb.group({
					meterId: [element2.meterId ],
					attributeName: [element2.attributeName, Validators.required],
					effect: [element2.effect, Validators.required]
				});
				(op.get("consequences") as FormArray).push(attr)
			});
			
		});
		this.postsForm.patchValue(data3);
	}

	goToLink(){
		this.router.navigate(['create']);
	}


	///////////////Getters and setters //////////////////////

	get options(){
		return this.postsForm.get('options') as FormArray;
	}
	
	get effect(){
		return this.testMeterAttr.get('effect');
	}

	get graphData() {
		return this.postsForm.get('graphData');
	}

	get graphType() {
		return this.postsForm.get('graphType');
	}

	get graphImage(){
		return this.postsForm.get('graphImage');

	}

}


