import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SceneBuilderComponent } from './scenebuilder.component';

describe('SceneBuilderComponent', () => {
  let component: SceneBuilderComponent;
  let fixture: ComponentFixture<SceneBuilderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SceneBuilderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SceneBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
