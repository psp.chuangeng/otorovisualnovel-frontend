import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { ConsoleReporter } from 'jasmine';
import {ImageUploadService} from "../../../services/image-upload.service";
import { timer } from 'rxjs';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { FormService } from 'src/app/services/form.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {

  
  @ViewChild('myFile', { static: false }) public myFile?: ElementRef;
  
  fileObj!: File;
  fileBase64: any; 
  fileUrl!: string;
  disabledUpload: boolean;
  selectedTypes: any;
  imageForm!: FormGroup;
  currentType: string; 
  showFileUpload: boolean; 
  spinning: boolean; 
  uploadSuccess: boolean; 
  uploadFailed: boolean; 
  isLoggedIn?: boolean;
  showUploadPage: boolean;
  showViewPage: boolean;
  errorDisplay: string; 
  
  showDataSet: any[];
  showImages: any[];

	displayedColumns!: string[];
	dataSource: any;
  disableBarChart: boolean;
  openChartModal: boolean;
  imageDeleted: string;
  deleteSuccess: boolean;
  deleteFailed: boolean;
  openInfoModal: boolean; 
  countdown: number;
  errorMsg: string = "";
  getImageFailed: boolean = false;
  filePath: string = "";
  openDataModal: boolean = false; 
  length: number = 0;


  constructor(private fileService: ImageUploadService,  private fb: FormBuilder, private router: Router, private tokenStorageService: TokenStorageService, private formService: FormService, private sanitizer: DomSanitizer) {

    //after upload show button to uplaod to s3 
    this.disabledUpload = false; 
    this.showFileUpload = false; 

    this.spinning = false;
    this.uploadFailed = false; 
    this.uploadSuccess = false;
    this.showUploadPage = false; 
    this.showViewPage = false; 
    this.errorDisplay = ""; 
    this.showDataSet = [];
    this.showImages = [];
    this.imageDeleted = "";
    this.deleteSuccess = false; 

    this.disableBarChart = true; 
    this.openChartModal = false; 
    this.deleteFailed = false; 
    this.openInfoModal = false;
    this.countdown = 10;



    this.selectedTypes = [{
      fileType: 'None',
      visual: 'None'
    },
    {
      fileType: 'graph',
      visual: 'Graph Image'
    },
    {
      fileType: 'graphData',
      visual: 'Graph Data'
    },
    {
      fileType: 'background',
      visual: 'Background Image'
    },
    {
      fileType: 'character',
      visual: 'Character Image'
    },
  ]

    this.currentType = "";


  }
  
  public getSantizeUrl(url : string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
}

  ngOnInit(): void {

    //this.errorMsg = "HELLO"
    //this.getImageFailed = true; 
    //this.spinning = true;
    //this.uploadSuccess = true; 
    //this.uploadFailed = true; 
    // this.errorMsg = "Please upload only jpg, jpeg, png, svg or gif format.";
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (!this.isLoggedIn) {
     this.router.navigateByUrl('/login');
    }


    this.imageForm = this.fb.group({
      fileType: ['', Validators.required]
    })

    this.imageForm.statusChanges.subscribe((e: string) => {
      let value = this.imageForm.value.fileType;
      //this.disabledUpload = false; //on changes of the dropdown, disable the upload button 
      //this.disableBarChart = true; //on changes of the dropdown, disable the show graph button
      this.filePath = "";

      if (value !== "None"){
        this.selectedTypes.forEach((element:any) => {
          if (element.visual == value){
            this.currentType = element.fileType;

            this.showFileUpload = true; 
          } 
        });
      } else {
        this.showFileUpload = false;
        //this.disabledUpload = false;
      }
    })


    this.imageForm.valueChanges.subscribe((data: any) => {
      if(this.myFile) {
        //console.log('clear my file')
        //console.log(this.myFile.nativeElement.files);
        
        this.myFile!.nativeElement.value = '';

        this.fileObj = new File([""], "");

        this.filePath = "";
        //console.log(this.fileObj)
        //console.log(this.fileObj.name)
        this.disabledUpload = false;
        this.disableBarChart = true; //on changes of the dropdown, disable the show graph button

        //console.log(this.myFile.nativeElement.files);
      }
    })

  }

  toggleGraphInfo(){
    this.openInfoModal = !this.openInfoModal;
  }

  closeInfoModal(){
    this.openInfoModal = !this.openInfoModal;
  }

  onFilePicked(event: Event): void { // staging 
    //console.log(this.currentType)

    //console.log(event.target)
    const FILE = (event.target as HTMLInputElement).files![0];
    this.fileObj = FILE;
    // this.errorMsg = "";
    console.log(this.fileObj)
    console.log(this.fileObj['type'])
    console.log(this.fileObj.type)


    //console.log(this.currentType)
    if (this.currentType == 'graph' || 'attribute' || 'character' || 'background') {

      if (!this.fileObj.name.match(/.(jpg|jpeg|png|gif|svg)$/i)) {

        

        this.errorMsg = "Please upload only jpg, jpeg, png, svg or gif format."
        this.myFile!.nativeElement.value = ''; // remove val from file 
        this.disabledUpload = false

        timer(4000).subscribe(
          val => this.errorMsg = ""
        )

        //timer(1000).subscribe(
        //  val => this.myFile!.nativeElement.files[0].name = ""
        //)

        } else {
        this.disabledUpload = true;

        const fileReader = new FileReader()


        /////////////////////////

        fileReader.readAsDataURL(this.fileObj);
        fileReader.onload=(e)=>{
        this.fileBase64 = e.target?.result;

        //show image 

        
        this.filePath = fileReader.result as string;

        console.log(this.filePath)
        
        }
      }
    } 
    
    if (this.currentType === 'graphData'){

      if (!this.fileObj.name.match(/.(xlsx)$/i)) {
        this.errorMsg = "Please upload only xlsx format."; 

        this.myFile!.nativeElement.value = ''; // remove val from file 
        this.disabledUpload = false
        this.disableBarChart = true;

        timer(4000).subscribe(
          val => this.errorMsg = ""
        )


      } else {
      this.disabledUpload= true;
      this.errorMsg = ""
      this.disableBarChart = true;

      const fileReader = new FileReader()
      fileReader.readAsDataURL(this.fileObj);
      fileReader.onload=(e)=>{
        this.fileBase64 = e.target?.result;
        //console.log(this.fileBase64)
      }
    }
  }
}

toggleShowUploadPage(){
  //console.log('hi')
  this.showUploadPage = true; 
  this.showViewPage = false;
  this.showImages = [];
}


toggleShowViewPage(){
  this.showViewPage = true; 
  this.showUploadPage = false;


  this.fileService.getImagesFromDB().subscribe({

    next: (res : any) => {

      let data = JSON.parse(res.body);

      data.forEach((e: any) => {
  
        if (e.imageUrl.match(/.(jpg|jpeg|png|gif|svg)$/i)) {
  
          
          let output = e.imageUrl.split("/");
          let imageName = output[output.length - 1];
          //console.log()
          
          this.showImages.push({type: 'picture', imageType: e.imageType, name: imageName, url: e.imageUrl })
        }
  
        if (e.imageUrl.match(/.(xlsx)$/i)){
  
          let output = e.imageUrl.split("/");
          let imageName = output[output.length - 1];
          //console.log()
  
          this.showImages.push({type: 'excel', imageType: e.imageType, name: imageName, url: e.imageUrl })
        }
  
      })

    },
   error: error => {

    this.getImageFailed = true; //leave it, feel shld not have timer for this
   
  }


  })

}

  deleteFile(delObj: any){
    //console.log(delObj)

    //{type: "picture", name: "character", 
    //url: "https://otoro-image-gallery.s3-ap-southeast-1.amaz…haracter/Screenshot 2020-05-26 at 10.44.51 AM.png"}

    //delete form db first 
    let output = delObj.url.split("/");

    //i am sure will have a folder and the url string png 
    let bodyS3 = {
      "key": output[output.length - 2] + '/' + output[output.length - 1],
      "httpMethod": "DELETE",
    } 

    this.spinning = true; 

    this.fileService.deleteImageFromDB(delObj.url, delObj.type).subscribe({
      next: (res: any) => {

        this.imageDeleted = output[output.length - 1];
        //console.log(res)
        if (res.status == 200) {
  
          this.fileService.deleteImageFromS3(bodyS3).subscribe({
            next: (resS3: any) => {
              if (resS3.status == 200) {
              
  
                //pass work flow 
                timer(2000).subscribe(
                  val => {
                    this.spinning = false;
                    this.deleteSuccess = true
    
                    timer(2000).subscribe(
                      val => {
                        this.imageDeleted = '';
                        this.deleteSuccess = false
                        //reload the page 
                        let currentUrl = this.router.url;
                        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
                        this.router.onSameUrlNavigation = 'reload';
                        this.router.navigate([currentUrl]);
                      }
                    )
                  }
                )

                
            }
          }, error: error => {

            console.log('failed on db')
            timer(2000).subscribe(
              val => {
                this.spinning = false;
                this.deleteFailed = true;
                timer(10000).subscribe(
                  val => {
                    this.deleteFailed = false;
                    this.imageDeleted = '';
                  }
                )
              })

          }
        })
      }
    }, error: error => {

      console.log('failed on s3')
      timer(2000).subscribe(
        val => {
          this.spinning = false;
          this.deleteFailed = true;
          timer(10000).subscribe(
            val => {
              this.deleteFailed = false;
              this.imageDeleted = '';
            }
          )
        })
    }
  }
  )
   

    //once success then delete from s3
    
    //reload 
  }

  //left meter , char and graph data for error checking 
  //upload xlsx data and test the interactive graph before uplaoding 
  //submit img / data and retireve response that upload to s3 is a success 
  //Allow this space to either upload / delete a list of data 

  onFileUpload() { // when upload file 
    if (!this.fileObj) {
        //this.errorImgMsg = true;
        //this.errorDataMsg= true;
        return
      }

      console.log(this.fileObj)
      //concatenate file name if got space 
      let name: string = this.fileObj.name.replace(/ +/g, "");
      console.log(name)

      console.log(this.fileObj)

      this.spinning = true // all starts with this

      //check if s3 have existing name 
      this.fileService.getAllObjectsFromS3().subscribe({

        next: (s3Images: any ) => {

          console.log(s3Images)
          if (s3Images.statusCode == 200) {
            if (s3Images.url.includes(this.currentType + '/' + name)) {
              console.log('File name taken S3 side, please name another name')
              this.spinning = false;
              this.errorMsg = "File name taken, please choose another name for your file.";
              console.log(this.errorMsg)
  
              timer(4000).subscribe(
                val => {
                  {
                    this.errorMsg = "";
                    //this.spinning = false; 
                  }
                }
              )
  
            } else { 
              console.log('carry on, check on db this time')
              this.fileService.getImagesFromDB().subscribe({
                next: (dbImages: any) => {
  
                  if (dbImages.status == 200) {
  
                    let  result = JSON.parse(dbImages.body).map( (item: any)=> { return item.imageUrl }); // get values of the specific keys only 
                    
                    console.log(result);
      
                    if (result.includes(this.currentType + '/' + name)) {
                      console.log('File name taken DB side, please name another name')
                      this.spinning = false;
                      this.errorMsg = "File name taken, please choose another name for your file.";
      
                      timer(4000).subscribe(
                        val => {
                          this.errorMsg = "";
                          //this.spinning = false; 
                        }
                      )
      
      
                    } else {
                      //can send to db this time
                      console.log('pipeline to send to db')
      
                      console.log(this.fileObj.type)
                      console.log(this.fileObj['type'])
                      console.log(name, this.fileObj.type , this.currentType) //this.fileBase64
                      
      
                      this.fileService.uploadfileAWSS3(name, this.fileObj.type, this.fileBase64 , this.currentType).subscribe({
                        next: (res: any) => {

                          if (res.statusCode == '200') {
                            console.log(res.url);
                            this.fileService.uploadToDB(res.url,this.tokenStorageService.getUser().id,this.currentType).subscribe({
                              next: (response:any) => {

                                if(response.status == 200){
        
                                  //pass work flow 
                                  timer(2000).subscribe(
                                    val => {
                                      this.spinning = false;
                                      this.uploadSuccess = true;
          
                                      timer(2000).subscribe(
                                        val => {
                                          this.uploadSuccess = false
          
                                          let currentUrl = this.router.url;
                                          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
                                          this.router.onSameUrlNavigation = 'reload';
                                          this.router.navigate([currentUrl]);    
                                        }
                                      )
                                    }
                                  )
          
          
                                } 
                                

                            }, error: error =>{

                            //fail work flow 
                            timer(2000).subscribe(
                            val => {
                            this.spinning = false;
                            this.uploadFailed = true;

                            timer(10000).subscribe(
                            val => {
                            this.uploadFailed = false;
                            })})
                        
                        }})
        
                          }
                          
                      }, error: error => {

                          //fail work flow 
                          timer(2000).subscribe(
                            val => {
                            this.spinning = false;
                            this.uploadFailed = true;

                            timer(10000).subscribe(
                            val => {
                            this.uploadFailed = false;
                            })})

                    }
                  })
                  }
                } 

              }, error: error => { 

              //fail work flow 
              timer(2000).subscribe(
                val => {
                this.spinning = false;
                this.uploadFailed = true;

                timer(10000).subscribe(
                val => {
                this.uploadFailed = false;
                })})


              
              }
              })
            }
          } 

        
      }, error: error => {

      //fail work flow 
      timer(2000).subscribe(
        val => {
        this.spinning = false;
        this.uploadFailed = true;

        timer(10000).subscribe(
        val => {
        this.uploadFailed = false;
        })})

      }})

    }


      dataFromEventEmiiter($event: any){ //can be use for uploading checks

        if ($event != 'error') {

          if (this.currentType === 'graphData') {

            const keys = Object.keys($event[0]);
            //get the end of the array and store it somewhere
            //{key: string, value: string}
            /// 
            const labels:  any= $event[$event.length - 1];
            $event.pop();
            //console.log(labels); 
            //////
  
            this.displayedColumns = keys; 
  
            //console.log($event);
            
            //console.log(this.displayedColumns);
  
            this.length = keys.length;
  
            this.dataSource = $event;
            this.formService.changeMessage($event, labels); // important to pass over 
            this.disableBarChart = false;
  
  
            //this.postsForm.get('excel')?.setValue($event);
          }
        } else {
          this.errorMsg = "Please adhere to the data format";

          timer(4000).subscribe(
            val => this.errorMsg = ""
          )

        }

      }

      openChart() {
        this.openChartModal = true;
        //const dialogRef = this.dialog.open(ChartComponent);
    
        //dialogRef.afterClosed().subscribe(result => {
        //	console.log(`Dialog result: ${result}`);
        //});
      }

      openDataTable(){
        this.openDataModal = true; 
      }

}
