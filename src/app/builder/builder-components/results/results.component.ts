import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';
import { StoriesService } from 'src/app/services/stories.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  
  @Input() gameCode?: any;
  // @Input() results?: any;
  meters:any;
  results?:any;

  users: any[];

  constructor(private gameService: StoriesService , private router: Router){
    this.users = [];
    
  }

  ngOnInit(): void {

    this.gameService.gameResult(this.gameCode).subscribe((res:any) => {
      if (res.status == 200){
        this.results = JSON.parse(res.body)
        console.log(this.results)

        this.users = this.results.players;
        this.meters = this.results.players[0].playerScore
        console.log(this.users)
        console.log(this.meters)

    }
  }
    )
}
  
  
    
    // this.users = this.results.players;
    // this.meters = this.results.players[0].playerScore
    // console.log(this.users)
    // console.log(this.meters)

     
  //  this.users = [{
  //    //id: 0,
  //    name: 'jenny',
  //    score: 10
  //  },
  //  {
  //    //id: 0,
  //    name: 'kenny',
  //    score: 20
  //  },
  //  {
  //    //id: 0,
  //    name: 'cenny',
  //    score: 110

  //  }]
  //}



  //goBackToStories(){
  //  console.log("Go back")
  //}
  

  refresh(){
  // let currentUrl = this.router.url;
  //   // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  //   // this.router.onSameUrlNavigation = 'reload';
  //   // this.router.navigate([currentUrl]);
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate([currentUrl]); // navigate to same route
  // });
  // }
  this.users = []
  this.meters = []
  this.gameService.gameResult(this.gameCode).subscribe((res:any) => {
    if (res.status == 200){
      this.results = JSON.parse(res.body)
      console.log(this.results)

      this.users = this.results.players;
      this.meters = this.results.players[0].playerScore
      console.log(this.users)
      console.log(this.meters)

  }
})




}

}


  



 