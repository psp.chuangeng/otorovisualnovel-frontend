import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { FormService } from 'src/app/services/form.service';
import { StoriesService } from 'src/app/services/stories.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-storyline',
  templateUrl: './storyline.component.html',
  styleUrls: ['./storyline.component.css']
})
export class StorylineComponent implements OnInit , AfterViewInit{

  openIntro: boolean; 
  content?: string;
  animal?: string; 
  name?: number; 
  stories?: any; 
  selectedId?: number;
  isValid: boolean; 
  openHostModal: boolean; 
  data: any;
  isLoggedIn?: boolean;
  gameCodes: any;
  openList: string;
  index?: number;
  openResultModal: boolean; 
  gameIDtoResult: any;
  results: any;
  openStoryBuilder: boolean;  
  openDeleteConfirmation: boolean;
  storyTitleToBeDeleted: string;
  disabledStoryTitle?: boolean = false;
  errorModal: boolean = false;
  errorMsg: string = "";
  spinning: boolean = false;
  deleteSuccess: boolean = false;

  constructor(private storyService: StoriesService, private router: Router, private tokenStorageService: TokenStorageService, private formService: FormService) {
    this.isValid = false; 
    this.openHostModal = false;
    this.openList = "close";
    this.openResultModal = false; 
    this.openStoryBuilder = false;
    this.openIntro = true;
    this.storyTitleToBeDeleted = '';
    this.openDeleteConfirmation = false; 
  }

  showAllCodes(s: any, index: number){

    // this.gameCodes = s.gameCodes;
    this.storyService.getCodesByStory(s.storyTitle).subscribe({
      next: (res:any) => {
        if (res.status == 200){
          this.gameCodes = Object.keys(JSON.parse(res.body));
          console.log(res)
          console.log(this.gameCodes)
        }
    }, error: error => {
      //error --> cant host game, error to admin 

      timer(2000).subscribe(
        val => {
          
        this.errorModal = true;

        this.errorMsg = "Unable to get game codes." ;

        timer(10000).subscribe(
        val => {
          this.errorModal = false; 
          this.errorMsg = "";
        })})


    }})

    this.index = index;
    this.openList == "close" ? this.openList = "open" : this.openList = "close"; 
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

  
    if (!this.isLoggedIn) {
    this.router.navigateByUrl('/login');
    }
    
    this.storyService.getStories().subscribe({

      next: (data: any) => {
        if (data.status == 200 && data.body != null) {
          this.stories = JSON.parse(data.body)    
      }
     }, error: error => {
       //cannot get stories -> contact admin 

       this.errorModal = true; 
       this.errorMsg = "Unable to get stories.";
    }
  });
}

  ngAfterViewInit(){

  }

  editStory(s: any){



    this.formService.storyFrmDBtoLocalStorage(s);    

    //open the modal of the builder 
    this.openStoryBuilder = true; 
    this.openIntro = false; 
    this.disabledStoryTitle = true; 
  }


  deleteStory(){

    this.spinning = true;

    this.storyService.deleteStory(this.storyTitleToBeDeleted).subscribe(
      {next: (res: any) => {
      if (res.status == 200){

        this.openDeleteConfirmation = false;

        //pass work flow 
        timer(2000).subscribe(
          val => {
            this.spinning = false;
            this.deleteSuccess = true;  

            timer(2000).subscribe(
              val => {
                this.deleteSuccess = false;
                //reload the page 
                let currentUrl = this.router.url;
                this.router.routeReuseStrategy.shouldReuseRoute = () => false;
                this.router.onSameUrlNavigation = 'reload';
                this.router.navigate([currentUrl]);
              }
            )
          }
        )
 

      }
    }, error: error => {
      //error handling here


      timer(2000).subscribe(
        val => {
          
        this.spinning = false;
        this.errorModal = true;
        this.errorMsg = "Unable to delete story " + this.storyTitleToBeDeleted + ".";
        timer(10000).subscribe(
        val => {
          this.errorModal = false; 
          this.errorMsg = "";
        })})


    }
  }
    )  

  }


  confirmDelete(s: any){
    console.log(s)

    this.openDeleteConfirmation = true;
    this.storyTitleToBeDeleted = s.storyTitle;
  
  }
  


  test(gameStoryNo: number){
    console.log(gameStoryNo);
    //activate button
  }

  showPastGames(e: any){
    console.log(e)
  }


  openDialog(s: any): void {
    this.openHostModal = true;
    this.openList = 'close';
    // this.data = {id: s.id, storyName: s.story};
    this.data = {storyTitle: s.storyTitle};
  }


  showResultsModal(a: any){

    this.openList = 'close';
    console.log(a)
    //call api to get the resulst
    

    //if successful, pass the game id and also the ongoing results over 
    this.gameIDtoResult = a; 
    this.openResultModal = true;
          
    // this.results = this.gameIDtoResult;

  

    // ...

  this.storyService.gameResult(this.gameIDtoResult).subscribe({
    next: (res: any) => {
      if (res.status == 200){
        this.results = JSON.parse(res.body)
        console.log(this.results)
    }
  }, error : error => {
    //error handling here 
        
      this.errorModal = true;
      this.errorMsg = "Unable to get game result " + this.storyTitleToBeDeleted + ".";
      
      timer(10000).subscribe(
      val => {
        this.errorModal = false; 
        this.errorMsg = "";
      })


  }
 })




            //show the modal 
       

   
    

    // this.results =  [{
    //   //id: 0,
    //   name: 'jenny',
    //   score: 10
    // },
    // {
    //   //id: 0,
    //   name: 'kenny',
    //   score: 20
    // },
    // {
    //   //id: 0,
    //   name: 'cenny',
    //   score: 110

    // }];
    
    
    //show the modal 
    // this.openResultModal = true;


  }




}

