import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import {ImageApiResponse} from '../models/image-api-response';
import {} from '../models/scene-api-response';
import * as XLSX from "xlsx";

//for testing purposes
import {ATTRIBUTE} from "./json-test/attribute";
import {BACKGROUND} from "./json-test/background";
import {CHARACTER} from "./json-test/character";
import {GRAPH} from "./json-test/graph";
import {GRAPHDATA} from "./json-test/graph-csv";
import { StoryApiResponse } from '../models/story-api-response';
import { STORY } from './json-test/story';
import {GameApiResponse, Scene} from '../models/scene-api-response';
import { thumbsDownIcon } from '@cds/core/icon';

import { environment } from '../../environments/environment';
import { TokenStorageService } from './token-storage.service';
import { GameService } from './game.service';



var api = environment.apiUrl //api general endpoint 

@Injectable({
  providedIn: 'root'
})
export class FormService {

  //for labels
  private labelSource = new BehaviorSubject({});
  currentLabels = this.labelSource.asObservable();

  //for data 
  private messageSource = new BehaviorSubject('');
  currentMessage = this.messageSource.asObservable();
  private disabled = new BehaviorSubject<boolean>(false);
  currentDisabled = this.disabled.asObservable();

  private _story: GameApiResponse = <GameApiResponse>{};
  private _scene: GameApiResponse["scenes"] = <GameApiResponse["scenes"]>[];
  private _storyTitle: GameApiResponse['storyTitle'] = '';
  localStorage: Storage;

  constructor(
    private http: HttpClient, private tokenService: TokenStorageService,) { 
      this.localStorage = window.localStorage;
  }

  
  getExcelDataFromUrl(data: string){ //get sheet of json from s3 xlsx url  
    return this.http.get(data, {responseType: 'arraybuffer'});
  }


  //excelData(data: string){
  // this.getExcelDataFromUrl(data).subscribe((res: any) => {
  //    var data = new Uint8Array(res);
  //    var arr = new Array();
  //    for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
  //    var bstr = arr.join("");
  //    /* Call XLSX */
  //    var workbook = XLSX.read(bstr, {type:"binary"});

  //    /* DO SOMETHING WITH workbook HERE */
  //    var first_sheet_name = workbook.SheetNames[0];
  //    /* Get worksheet */
  //    var worksheet = workbook.Sheets[first_sheet_name];
  //    let x = XLSX.utils.sheet_to_json(worksheet,{raw:true});
  //    console.log(x)
  // })
  //}
  
  getStory(): GameApiResponse{
    // return this._story;
      return JSON.parse(this.localStorage.getItem("story") || '[]');
  }

  submitStory(value: any){

    value.userId = this.tokenService.getUser().id;

    value["scenes"].forEach((val: any, index:number) => {
      val['stackNo'] = index+1;
    })

    console.log(value)

    // console.log(this.getStory());

    // alter number to string format to send to backend

    return this.http.post(api+ "test/postScenes", JSON.stringify(value), { observe: 'response', responseType: 'text'})

  }

  //for storybuilder and scenebuilder
  addStory(story: GameApiResponse) {
    this._story = story;
    // console.log('testing from addstory');
    // console.log(this._story);
    this.localStorage.setItem("story", JSON.stringify(this._story));
    // this.getStory().scenes = localStorage.getItem("storyScenes");
  }

  storyFrmDBtoLocalStorage(data: any){

    let attribute = data.attributes; 

    let title = data.storyTitle;

    let winningLosingScenes: any[] = [];
    console.log(data.scenes.length)
    data.scenes.forEach((scene: any) => {

      if (scene.sceneType == 'winning') {
        winningLosingScenes.push(scene)
      } 

      if (scene.sceneType == 'losing') {
        winningLosingScenes.push(scene)
      } 
    })

    //take out winning and losing scenes 
    let filtered_scene = data.scenes.filter((item: any, i:any) => item.sceneType !== 'losing' && item.sceneType !== 'winning');

    console.log('attribute', attribute);
    console.log('title', title);
    //console.log('scenes', scenes);
    console.log('winninglosing', winningLosingScenes)

    console.log('filtered', filtered_scene);

    //segregate them 
    
    //story
    //attributes 
    this.localStorage.setItem("story", JSON.stringify(data));
    this.localStorage.setItem("storedScenes", JSON.stringify(filtered_scene));

    this.localStorage.setItem("storedAttributes", JSON.stringify(attribute))

    //this.localStorage.setItem("storedAttributes", JSON.stringify(attribute));
    //this.localStorage.setItem("storedScenes", JSON.stringify(filtered_scene));
    this.localStorage.setItem("storedWinLoseScenes", JSON.stringify(winningLosingScenes));
    //this.localStorage.removeItem("storedScenes");
    //this.localStorage.removeItem("storeToScene");
    //this.localStorage.removeItem("storedWinLoseScenes");
    //this.localStorage.removeItem("storedStoryTitle");
    //this.localStorage.removeItem("storedAttributes");
  }

  addStoryTitle(storyTitle: GameApiResponse["storyTitle"]){
    // this._storyTitle = storyTitle;
    // console.log(this._storyTitle);
    // let localStorageStoryTitle = this.getStory().storyTitle;
    // var storedStoryTitle = this.getStory().storyTitle;
    this.localStorage.setItem("storedStoryTitle", storyTitle);
    // console.log(this.localStorage.getItem("storedStoryTitle"));
    // this.getStory().scenes = localStorage.getItem("storyScenes");
    // console.log(this.getStory());

  }

  //send to backend 
  combineJSON(){
    let story = this.getStory();
    let scenes = this.getScene();
    let WinLoseScene = this.getStoredWinLoseScenes();
    story.scenes = scenes;
    WinLoseScene.forEach((element: any) => {
      story.scenes.push(element);
    });


    //for stack no - new  
    story["scenes"].forEach((val: any, index:number) => {
      val['stackNo'] = index+1;
    })
    ////

    let attrlist:any = [];
    story.attributes.forEach((att: any) => {

      attrlist.push({'attributeName': att.attributeName, 'attributeImage': '','winningScore': att.winningScore, 'attributeDescription': att.attributeDescription})

    })

    story.attributes = attrlist;
    console.log(story);
    return story; 
  }

  //GameApiResponse["scenes"]
  updateFinalScene(scene: any, index: number){
    let currentFinalScene = this.getStoredWinLoseScenes();
    console.log(this.getStoredWinLoseScenes())

    if (scene.sceneType == "winning") {

    let index = currentFinalScene.findIndex((x: any) => x.sceneType =="winning");
    currentFinalScene[index] = scene;

    } else {

      let index = currentFinalScene.findIndex((x: any) => x.sceneType =="losing");
      currentFinalScene[index] = scene;

    }

    //currentFinalScene[index] = scene;
    this.localStorage.setItem("storedWinLoseScenes", JSON.stringify(currentFinalScene));
     console.log(this.getStoredWinLoseScenes());
  }

  updateScenes(){
    if (this.getScene().length > 0){
      //compare differences between current scene attribute (currentScene) and updated Attribute (checkScene)
      let currentScene = this.getScene();
      let updatedMeter = this.getAttribute();
      var count = 0;
      var checkSceneCount = 0;
      let checkScene:any = [];
      let oldMeter: any = [];
      var oldMeterCount =0;
      currentScene.forEach((s:any) => {
        // if (s)
        s.options.forEach((o:any) => {
          o.consequences.forEach((element: any) => {
            if (oldMeterCount <4){
              oldMeter.push({meterId: oldMeterCount, attributeName: element.attributeName, effect: element.effect})
              oldMeterCount ++;
            }
          });
          // oldMeterCount = 0;
        });
        // oldMeterCount = 0;
      });

      updatedMeter.forEach((u: any) => {
        checkScene.push({meterId: checkSceneCount, attributeName: u.attributeName, effect: 0})
        checkSceneCount ++;
      });
      //console.log(updatedMeter);
      //console.log(checkScene);
      // console.log(currentScene.options.length);
      
      //console.log(currentScene);
      currentScene.forEach((element:any) => {
        
        element.options.forEach((o: any) => {
          // if updated meter array count more than current scene options.consequences, push empty name into array
          if (checkScene.length > oldMeter.length){
            //console.log("more than");
            //console.log(checkScene.length);
            // console.log(o.consequences.length);
            
              checkScene.forEach((c:any) => {
                // console.log(o.consequences);
                //console.log(c);
                if (oldMeter.some((code:any) => code.meterId === c.meterId)===false){
                  if (oldMeter.length < 3 && oldMeter.length<checkScene.length){
                    //console.log('printing c')
                    //console.log(c)
                    oldMeter.push(c)
                    
                  }
                }
                // count ++;
              });
            
              o.consequences = oldMeter;

          // if updated meter array count less than current scene options.consequences, remove json string by attributeName that is not in.
          } else if(checkScene.length < oldMeter.length){
            //console.log("less than");
            //console.log(checkScene.length);
            // console.log(o.consequences.length);
            let newConsequences:any = [];
            let count2=0;
            oldMeter.forEach((c:any) => {
              // console.log(c);
              // console.log(o.consequences);
              if (checkScene.some((code:any) => code.attributeName === c.attributeName)===true){
                // console.log(o.consequences[count])
                newConsequences.push({meterId: count, attributeName: oldMeter[c.meterId].attributeName, effect: o.consequences[c.meterId].effect})
                count ++;
                } 
              count2 ++;
            }); 
            // console.log(o.consequences);
            o.consequences = newConsequences;
            // console.log(o.consequences);
          // if updated meter array count is equal to current scene options.consequences, update meter name in scene through index
          } else if(checkScene.length == oldMeter.length){
            //console.log("equal to");
            //console.log(checkScene.length);
            //console.log(o.consequences.length);
            // if(checkScene)
            for (let i = 0; i < checkScene.length; i++) {
              //console.log("test");
              if (oldMeter[i].meterId == checkScene[i].meterId){
                //console.log(o.consequences[i].attributeName);
                o.consequences[i].attributeName = checkScene[i].attributeName;
              };
            }
          }
          count = 0;
        })
      });
    
        localStorage.setItem("storedScenes", JSON.stringify(currentScene));
        //console.log(this.getScene());
    }

  }

  addSceneContent(scene: GameApiResponse["scenes"], stackNo: number){
    let currentScenes: any[] = this.getScene();
    // console.log(stackNo);
    // console.log(currentScenes);
    currentScenes[stackNo] = scene; ;
    // console.log(currentScenes[stackNo]);
  
    this.localStorage.setItem("storedScenes", JSON.stringify(currentScenes));

    //check if all the scences are filled up
    let status = this.checkScenesStatus();
    return status;

  }

  addScenes(scene: any){ // add empty scene to list 
    // if (this.getScene() != undefined){
      delete scene.value['stackNo'];
      // console.log(scene);
      // console.log(this.getScene());
      // console.log(typeof this.getScene());
      // let currentScenes: any = this.getScene();
      // console.log(this.localStorage.getItem("storedScenes"));
      if (this.localStorage.getItem("storedScenes") === null){
        this._scene.push(scene.value);
        this.localStorage.setItem("storedScenes", JSON.stringify(this._scene));
        // console.log("hello")
      } else{
        let currentScene = this.getScene();
        currentScene.push(scene.value);
        this.localStorage.setItem("storedScenes", JSON.stringify(currentScene));
        // console.log("hi")
      }
    
      
      //check if all the scences are filled up, error checking for the next button for scenes
      let status = this.checkScenesStatus();
      return status;
      // console.log(this._scene)
      // currentScenes.append(scene) // keep on appending
      
    // } 
    // else { //if scene is empty at the start
      // let storedScenes: any[] = [];
      // storedScenes.append(scene);
      // this.localStorage.setItem("storedScenes", JSON.stringify(storedScenes));
    // }
    // console.log(this.getScene());

  }

  checkScenesStatus(){ //check if all scenes are okay or not 
    let currentScenes: any = this.getScene();
    let sceneCheckerArray: boolean[] = []; 

    console.log(currentScenes);
    let disable_status: boolean = false;   

    if (currentScenes.length != 0 ) {
      currentScenes.forEach((data: any) => { 
        if (data.sceneTitle == "" && data.backgroundImage == "" && data.description == "") {
          //if blank , disable from progressing
          sceneCheckerArray.push(true); 
        } else {
          //everything is okay
          sceneCheckerArray.push(false);
        }
      }) 
    } else {
      sceneCheckerArray.push(true);
    }

    
    if (sceneCheckerArray.includes(true)) {
      disable_status = true
    } else {
      disable_status = false
    }

    return disable_status;
  }


  //only used once when refresh back the page 
  checkAttributeStatus(){
    let attributes = this.getAttribute();
    let attCheckerArray: boolean[] = []; 
    console.log(attributes);
    let disabled_att_button: boolean = false; 

    if (attributes.length != 0) {
      attributes.forEach((element: any) => {
        console.log(element)
        if (element.attributeName == "" && element.winningScore == "", element.attributeDescription == ""){
          attCheckerArray.push(true) 
        } else {
          attCheckerArray.push(false)
        }
      });

    }

    if (attCheckerArray.includes(true)) {
      disabled_att_button = true
    } else {
      disabled_att_button = false
    }

    return disabled_att_button;
  }

  checkWinLoseStatus(){
    let winLoseData = this.getStoredWinLoseScenes(); 
    let winChecker: boolean = false;
    let loseChecker: boolean = false;

    let returnBoolean: boolean[] = [];

    if (winLoseData == []) {
      return false
    } else {
      winLoseData.forEach((scene: any) => {

          if (scene.sceneType == "winning" && scene.backgroundImage == "" || scene.description == "" ) {
            winChecker = true // true means will disable the button 
          } 
          
          if (scene.sceneType == "losing" && scene.backgroundImage == "" || scene.description == "" ) {
            loseChecker = true
          } 

      })

      console.log(winChecker)
      console.log(loseChecker)


      returnBoolean.push(winChecker)
      returnBoolean.push(loseChecker)

      console.log(returnBoolean)

      return returnBoolean;

    }

    //if (winLoseChecker.includes(true)) {
    //  return true
    //} else {
    //  return false
    //}

  }

  getStoredWinLoseScenes(){
    return JSON.parse(this.localStorage.getItem("storedWinLoseScenes") || '[]');
  }

  addFinalScene(scene: GameApiResponse["scenes"]){
    let currentScenes: any[] = this.getStoredWinLoseScenes();
    // console.log(currentScenes);
    currentScenes.push(scene);
    
    this.localStorage.setItem("storedWinLoseScenes", JSON.stringify(currentScenes));
  }

  // removeMeter(index: number){
  //   let currentScenes: any[] = this.getScene();
  //   console.log(currentScenes);
  //   currentScenes.forEach((s: any) => {
  //     s.options.forEach((o:any) => {
  //       let filtered = o.consequences.filter((item: any, i:any) => i !== index);
  //       o.consequences = filtered;
  //     });
  //   });
  //   console.log(currentScenes);
  //   // let filtered = currentScenes.filter((item, i) => i !== index);
  //   this.localStorage.setItem("storedScenes", JSON.stringify(currentScenes));
  // }

  removeScene(index: number){
    let currentScenes: any[] = this.getScene();
    let filtered = currentScenes.filter((item, i) => i !== index);
    this.localStorage.setItem("storedScenes", JSON.stringify(filtered));

  

    //check if all the scences are filled up, error checking for the next button for scenes
    let status = this.checkScenesStatus();
    return status;

  }

  removeAllScenes(){
    //this.localStorage.removeItem("storedScenes");
    //this.localStorage.removeItem("storeToScene");
    //this.localStorage.removeItem("storedWinLoseScenes");
    //this.localStorage.removeItem("story");
    //this.localStorage.removeItem("storedStoryTitle");
    //this.localStorage.removeItem("storedAttributes");
    this.localStorage.clear();
  }

  removeFinalScene(){
    this.localStorage.removeItem("storedWinLoseScenes");
  }

  getScene(){
    return JSON.parse(this.localStorage.getItem("storedScenes") || '[]');
    // return JSON.parse(this.localStorage.getItem("story") || '{}');
  }

  getAttribute(){
    return JSON.parse(this.localStorage.getItem("storedAttributes") || '[]');
  }

  addAttributes(attributes: GameApiResponse["attributes"]){
    // console.log('test att', attributes);
    // var storedAttributes = this.getStory().attributes;
    //console.log(attributes);
    this.localStorage.setItem("storedAttributes", JSON.stringify(attributes));
    // console.log(this.localStorage.getItem("storedAttributes"));
    //console.log(this.getAttribute());
    this.updateScenes();
    // console.log(this.getStory());
    
  }

  // updateStory(scene: Scene){
  //   this._scene = scene;
  //   console.log('testing for scene')
  //   console.log(this._scene);
  //   this.localStorage.setItem("scene", JSON.stringify(this._scene));
  // }

  newString(){
    var newJSON = [];
    // newJSON.push(this.localStorage.getItem("storedStoryTitle"));
    // newJSON.push(this.localStorage.getItem("storedAttributes"));
    var storedStoryTitle = this.getStory().storyTitle;
    this.localStorage.setItem("storedStoryTitle", storedStoryTitle);
    var storedAttributes = this.getStory().attributes;
    this.localStorage.setItem("storedAttributes", JSON.stringify(storedAttributes));
    newJSON.push(this.localStorage.getItem("storedScenes"));
    // console.log(newJSON);
    this.localStorage.setItem("story", JSON.stringify(newJSON));
    // console.log(this.getStory());
  }

  //{key: string, value: string}
  changeMessage(json: any, labels?: any) {
    this.messageSource.next(json);
    this.labelSource.next(labels); 
  }

  getStories(): Observable<StoryApiResponse>{
    return of(STORY);
  }


  getAttributeImages(): Observable<ImageApiResponse> {
    return of(ATTRIBUTE);
  }
    
  getBackgroundImages(): Observable<ImageApiResponse> {
    return of(BACKGROUND);
  }

  //getBackgroundByName(name: string) {
  //  return this.getBackground().pipe(
  //    map((background: ImageApiResponse) => background.find(background => background.name === name))
  //  );
  //}

  getCharacterImages(): Observable<ImageApiResponse> {
    return of(CHARACTER);
  }

  getGraphImages(): Observable<ImageApiResponse>{
    return of(GRAPH);
  }

  getGraphData(): Observable<ImageApiResponse>{
    return of(GRAPHDATA);
  }



  //getWeatherStatus(lat: number, lon: number):Observable<WeatherApiResponse> {
  //  return this.http.get<WeatherApiResponse>(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${this.apiKey}`)
  //}
  
  //getMovie(params: string){
  //  if (params === '') {
  //    return of([]);
  //  }
  //  return this.http.get(attribute);
  //}

  sendJSONStringForPreview(para : number){
    let scenes = this.getScene();
    let response : any = {}
    let attribute =  this.getAttribute()
    response["attributes"] = attribute
    let combined = Object.assign({}, response, scenes[para]);
    console.log(combined)
    return combined
  }

  JSONStringForFinalScenePreview(i: string){
    let finalscene = this.getStoredWinLoseScenes();
    let response : any = {}
    let attribute =  this.getAttribute()
    let empty = {};
    console.log(finalscene)
    response["attributes"] = attribute
    finalscene.forEach((element: any) => {
      if (element.sceneType==i){
        empty = element;
        console.log(empty)
      }
    });
    let combined = Object.assign({}, response, empty);
    console.log(combined)
    return combined
  }

}
