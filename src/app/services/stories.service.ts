import { Injectable } from '@angular/core';
import { GAMESTORIES } from './json-test/stories';
import { from, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GameStoriesResponse } from '../models/game-stories-api.response';
import { GAMECODERESULT } from './json-test/game-results';

import { GameApiResponse } from '../models/scene-api-response';
import { TokenStorageService } from './token-storage.service';
import { environment } from '../../environments/environment';


var api = environment.apiUrl + "test"; //api general endpoint 

//var api = 'localhost:8080'

@Injectable({
  providedIn: 'root'
})

export class StoriesService {

  userID: number; 
  constructor(private http: HttpClient, private tokenService: TokenStorageService) { 
    this.userID = this.tokenService.getUser().id
  }

  //get all storeis, render in the 

  getStoryFromDB():Observable<GameApiResponse> {
    return this.http.get<GameApiResponse>(api+'/getAllStories');
  }

  getCodeFromDB(comment: string, storyTitle: string){
    //for space on comment put %20 in between
    
    let altered_comment_for_db = comment.replace(/%20/g, " ");
    //console.log(api+/createGetCodeNow?comment=${altered_comment_for_db}&storyTitle=${storyTitle});

    return this.http.get(api+`/createGetCodeNow?comment=${altered_comment_for_db}&storyTitle=${storyTitle}&userId=${this.userID}`, { observe: 'response', responseType: 'text'});
  }



  //localHost 
    
  getStories() {
    // return of(GAMESTORIES); //get from backednd the stories 
    //console.log(userID)
    //return this.http.get(`http://localhost:8081/api/test/getAllStories?userId=${userID}`,  { observe: 'response', responseType: 'text' }) 

    //console.log(this.userID)
    return this.http.get(api+ `/getAllStories?userId=${this.userID}`,  { observe: 'response', responseType: 'text' })
  }

  sendClassName(){ //post class name and get the game code to start the game 
  // post story title, className 
  //response will be a string of the game code 
  }

  gameResult(gameCode: string){
    //get result by code 
    return this.http.get(api+`/getResultsByCode?code=${gameCode}`, {observe: 'response', responseType: 'text'});
    //return of(GAMECODERESULT);
  }

  getCodesByStory(storyTitle: string){

    return this.http.get(api+`/getAllCodesByStory?storyTitle=${storyTitle}&userId=${this.userID}`, {observe: 'response', responseType: 'text'});
  }

  deleteStory(storyTitle: string){

    return this.http.request('delete', api + `/deleteScenes?storyTitle=${storyTitle}&userId=${this.userID}`, { observe: 'response', responseType: 'text' } )
  }












}
