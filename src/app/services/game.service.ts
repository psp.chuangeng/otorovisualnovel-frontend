import { Injectable } from '@angular/core';
import {GameApiResponse} from '../models/scene-api-response';
import {GameResultPost} from '../models/game-result-api-post'
import { from, Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import {SCENE} from "./json-test/scene";
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { __values } from 'tslib';
import { FormService } from './form.service';
import { TokenStorageService } from './token-storage.service';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';


var websiteURL = environment.websiteUrl; //api general endpoint 
var apiURL = environment.apiUrl; //api general endpoint 




@Injectable({
  providedIn: 'root'
})
export class GameService {
  private attributeDict = new BehaviorSubject<any>({});
  private gameCodePrivate = new BehaviorSubject<string>("");
  private playerNamePrivate = new BehaviorSubject<string>("");
  private privateSceneList = new BehaviorSubject<any>("weee");
  private privateStoryDetails = new BehaviorSubject<any>({});
  currentSceneList = this.privateSceneList.asObservable();

  
  // this.sceneList[element.stackNo] = element
  // this.sceneLength = this.response.length -1;
  // this.storyDetails = this.response.attributes
  response : any = {}
  responseTest : any = []
  responseScene: any = {}

  tutorialList : any;
  sceneList : any = { };
  endSceneList : any = { };
  sceneListTest : any = [];
  sceneLength : number = 0;
  storyDetails : any = {};
  stackNo : number = 0;

  attributeResult = this.attributeDict.asObservable();
  gameCode = this.gameCodePrivate.asObservable();
  playerName = this.playerNamePrivate.asObservable();
  
  constructor(private http: HttpClient, private router: Router, private formService: FormService, private tokenStorageService: TokenStorageService) { 
    this.tutorialList = ["../assets/images/tutorial1.svg", "../assets/images/tutorial2.svg"];
  }
  createStackNo(sceneInput: any){
    var count = 1
    var tempSceneList = []
    var scene = {}
    for (let item of sceneInput) {
      
      item.stackNo = count
      scene = item
      tempSceneList.push(scene)
      count += 1
    }
    console.log(tempSceneList)
  }
  formatSceneList(sceneInput: any){
    // var sceneinput2 = JSON.parse(sceneInput);
    // console.log(sceneInput)
    for (let item of sceneInput) {
      if (item.sceneType == "normal"){
        this.sceneList[item.stackNo] = item 
      }
      else{
        this.endSceneList[item.sceneType] = item
        console.log(this.sceneList)

      }
    }
    // console.log(this.sceneList)
    console.log(this.endSceneList.losing)

  }      
  formatPreviewSceneList(sceneInput: any){
    console.log("formatpreview")
    console.log(sceneInput)
    sceneInput.forEach((element:any)  => {
      if (element.sceneType == "normal"){
        this.sceneList[element.stackNo] = element 
      }
      else{
        this.endSceneList[element.sceneType] = element
        console.log(this.sceneList)

      }
    });
    
    
    
    // for (let item of sceneInput) {
    //   if (item.sceneType == "normal"){
    //     this.sceneList[item.stackNo] = item 
    //   }
    //   else{
    //     this.endSceneList[item.sceneType] = item
    //     console.log(this.sceneList)

    //   }
    // console.log(this.sceneList)

  }
  getSceneBg(para: number){
    return this.sceneList[para].backgroundImage  
  }
  getWholeScene(para: number){
    return this.sceneList[para]  
  }
  getPreviewScene(){
    return this.sceneList[this.stackNo]  
  }
  getEndScene(para: string){
    if (para == "losing" ){
      console.log(this.endSceneList.losing)
      return this.endSceneList.losing 
    }
    else{
      console.log(this.endSceneList.winning)
      return this.endSceneList.winning 
    }
  }


  call(){
    this.formatSceneList(this.response.scenes)
    this.sceneLength = this.response.scenes.length -1;
    
    this.storyDetails = this.response.attributes
    console.log(this.storyDetails)
  }


  getSceneList(para : any) {
    //for local test
    //  this.http.get<GameApiResponse>(`https://gt8kjqse31.execute-api.ap-southeast-1.amazonaws.com/otoroproduction/api/test/getStoryByCode?gameCode=${para}`)
     return this.http.get( apiURL + `test/getStoryByCode?gameCode=${para}`,  { observe: 'response', responseType: 'text' })
 
  }
  loadFromLocal(para : any){
    this.response = para
    this.submitGameCode("GameCode");
    this.submitPlayerName("previewPlayer");
    this.sceneList[para.stackNo] = para  
    this.stackNo = para.stackNo
    this.storyDetails = this.response.attributes

  }
  previewScene(para : any){
    var parse = JSON.stringify(para)
    console.log(parse)
    localStorage.setItem("response", parse);
    this.router.navigate([]).then(result => {window.open(websiteURL + "preview-scene", '_blank'); });
    //for live server
    // this.router.navigate([]).then(result => {window.open("https://www.otoro.world/preview-scene", '_blank'); });    
  }

  previewGame(story: any){
    this.response = story;
    this.submitGameCode("GameCode");
    this.submitPlayerName("previewPlayer");
    
    console.log(story)
    localStorage.setItem("response", JSON.stringify(story));

    //this.formatSceneList(this.response.scenes )
    //this.sceneLength = this.response.length -1;
    //this.storyDetails = this.response.attributes
    //this.router.navigate([`${"preview-game"}`]);
    this.router.navigate([]).then(result => {window.open(websiteURL + "preview-game", '_blank'); });


    //localStorage.removeItem("response");
    //for live server
    // this.router.navigate([]).then(result => {window.open("https://www.otoro.world/preview-game", '_blank'); });    
  

  }
  

  getTutorialImages(para:number){
    if (para == this.tutorialList.count -1){
      return null
    }
    else{}
    return this.tutorialList[para]
  }
  
  submitPlayerName(para: string){
    this.playerNamePrivate.next(para)
  }
  submitGameCode(para: string){
    this.gameCodePrivate.next(para)
  }
  updateAttribute(para: any){
    this.attributeDict.next(para)
  }
}


