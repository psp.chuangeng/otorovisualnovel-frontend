import { GameStoriesResponse } from '../../models/game-stories-api.response';

export const GAMESTORIES: GameStoriesResponse = 

{
    "game": [
            {"id": 1, "storyTitle": "frontednv2", "gameCodes": [123 , 456 , 789]}, 
            {"id": 2, "storyTitle": "blues clues", "gameCodes": [555 , 666 , 777]},  
            {"id": 3, "storyTitle": "seseme street", "gameCodes": [0 , 45454 , 767]}
        ]
}