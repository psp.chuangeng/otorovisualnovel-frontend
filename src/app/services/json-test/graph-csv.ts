import {ImageApiResponse} from '../../models/image-api-response';


export const GRAPHDATA: ImageApiResponse = 

{
    "user": "user001",
    "imageType": "graph",
    "images": 
[
    {
        "name":  "graph1",
        "url": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/graphData/iris.xlsx"
    },
]
}