import {ImageApiResponse} from '../../models/image-api-response';


export const ATTRIBUTE: ImageApiResponse = 

{
    
    "user": "user001",
    "imageType": "attribute",
    "images": 
    [
        {
            "name":  "bar",
            "url": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/attribute/bar-chart.png"
        },
        {
            "name": "dish",
            "url": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/attribute/dish.png"
        },
        {
            "name": "gdp",
            "url": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/attribute/gdp.png"
        },
        {
            "name": "heart",
            "url": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/attribute/heartbeat.png"
        },

    ]
}
