import {StoryApiResponse} from '../../models/story-api-response';

export const STORY: StoryApiResponse = 

    {
        "userID": 1,
        "stories":[
            {
                "storyID": 10001,
                "storyTitle":"Otoro Advantures"},
            {
                "storyID": 10002,
                "storyTitle":"Otoro Adventures 2"}
            ]
    }