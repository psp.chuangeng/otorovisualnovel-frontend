import {ImageApiResponse} from '../../models/image-api-response';


export const BACKGROUND: ImageApiResponse = 

{
    "user": "user001",
    "imageType": "background",
    "images": 
    [
        {
            "name":  "city",
            "url": "http://d2cb9spvw8xnlm.cloudfront.net/background/city.jpg"
        },
        {
            "name": "office",
            "url": "http://d2cb9spvw8xnlm.cloudfront.net/background/cityscape.jpeg"
        },
        {
            "name": "classroom",
            "url": "http://d2cb9spvw8xnlm.cloudfront.net/background/classroom.png"
        },
        {
            "name": "dust",
            "url": "http://d2cb9spvw8xnlm.cloudfront.net/background/dust.jpg"
        },
        {
            "name": "hospital",
            "url" : "http://d2cb9spvw8xnlm.cloudfront.net/background/hospital.png"
        },
        {
            "name": "mrt", 
            "url" : "http://d2cb9spvw8xnlm.cloudfront.net/background/train.jpg"
        }
    ]
}
