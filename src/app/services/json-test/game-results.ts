import { RootObject } from "src/app/models/game-code-results";

export const GAMECODERESULT:  RootObject = 

{
	"gameid": "232",
	"players": [
	{
		"playerName": "player1",
		"playerScore": [
			{
				"attributeName": "happiness",
				"score": "4"
			},
			{
				"attributeName": "health",
				"score": "3"
			}
		]
	},
		{
		"playerName": "player 2",
		"playerScore": [
			{
				"attributeName": "happiness",
				"score": "10"
			},
			{
				"attributeName": "health",
				"score": "10"
			}
		]
	}
	]
}