import {GameApiResponse} from '../../models/scene-api-response';

export const SCENE: GameApiResponse = 

  
    {
        "storyId": "10001", 
        "storyTitle":"UAT Spread of Diseases",
        "attributes": [
            {
                "attributeName": "health",
                "attributeImage": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/attribute/bar-chart.png",
                "winningScore": 50,
                "attributeDescription": "Health is the indicator of your citizen's life expectacy and infant mortality rate, affected by diseases."
            },
            {
                "attributeName": "reputation",
                "attributeImage": "aws.com/happy.png",
                "winningScore": 50,
                "attributeDescription": "Your reputation reflects your capabilities as a minister and is affected by wrong choices you make."
            }          
        ],
                
        "scenes":[

        {
                "sceneId": "SC000",
                "stackNo": 1,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "You are the ministry of health official of a developed country, Utopia. Its climate is generally tropical and the city is very urbanised.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC000",
                "stackNo": 2,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": " Utopia’s population stands at 5.70 million and has an all high GDP of 372.1 billion USD. This is in stark contrast with its neighbouring countries like Cambodia, Congo and Nigeria which are LDCs and struggle to solve nationwide issues.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC000",
                "stackNo": 3,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Every decision you make will have a direct impact on your reputation, Utopia’s health, or the world’s health.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC000",
                "stackNo": 4,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "You will lose the game if you score lower than 50 points on either scores by the end!",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC001",
                "stackNo": 5,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "You are sitting in your office sorting out some of your paperwork while watching the news.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 6,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "There is a piece of emergency news regarding a disease outbreak in your neighbouring country.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 7,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Hmm... so the symptoms of the disease include headache, fever, nausea, vomiting, chills...",
                "characterName": "You",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 8,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "sweating, muscle pain, and fatigue...",
                "characterName": "You",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 9,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "While listening to the news, your assistant burst into your office.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 10,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Minister, there is an outbreak of Malaria within our neighbouring countries and we are starting to see a few reported cases in Utopia.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Malaria?",
                        "impactScene": "",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 11,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Is it the same disease shown on the news right now? With symptoms including headache and vomiting?",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 12,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Your assistant takes a look at the television.",
                "characterName": "",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 13,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Yes.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "How is this disease transmitted?",
                        "impactScene": "",
                        "consequences":[]
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 14,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "It is a vector-borne disease!",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC003",
                "stackNo": 15,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "I see, so it is transmitted by...",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Person to person via living organisms like mosquitos",
                        "impactScene": "Correct, vector-borne disease is transmitted from person to person via living organisms like mosquitos.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 10
                            }
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "Person to person via air",
                        "impactScene": "Wrong, vector-borne disease is transmitted from person to person via living organisms like mosquitos.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": -5
                            }
                        ]
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 16,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "It happens when the female mosquitoes get infected with Malaria when it takes blood from a human infected with Malaria parasites.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 17,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "The mosquitoes then bites another human and passes on the Malaria parasites.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 18,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "The parasite will then migrate to the liver where they reproduce and spread into the bloodstream.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Hmm... Will this disease affect Utopia?",
                        "impactScene": "",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC003",
                "stackNo": 19,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "What is your decision?",
                "characterName": "",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Call for extreme nationwide actions",
                        "impactScene": "Utopia is a DC and the country has less challenge with treating such infectious diseases due to its resources and socio-economic factors. A serious call for action is needed but extreme measures will be too unnecessary.",
                        "consequences": []
                    },
                    {
                        "optionId": "2",
                        "optionName": "Do nothing for now",
                        "impactScene": "Although the disease is not as threatening to a DC like Utopia, infectious diseases are to be taken seriously.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": -10
                            }
                        ]
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 20,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "So, based on our socio-economic factors, Malaria is less likely to be as widespread in our country... Overcrowded living conditions, with large numbers of people living very close together in a small area, tend to spread diseases quickly and easily.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC003",
                "stackNo": 21,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "And since Malaria is spread by mosquitoes, what are the conditions that mosquitoes will breed in?",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Wet environment",
                        "impactScene": "Yes! Mosquito eggs require the presence of water to hatch! However, they can still lay eggs in dry conditions. The eggs can lie dormant in dry conditions for up to nine months and after which, they may hatch if exposed to favourable conditions.",
                        "consequences": []
                    },
                    {
                        "optionId": "2",
                        "optionName": "Dry environment",
                        "impactScene": "Although mosquito eggs require the presence of water to hatch, they can still lay eggs in dry conditions. The eggs can lie dormant in dry conditions for up to nine months and after which, they may hatch if exposed to favourable conditions.",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 22,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "In conclusion, mosquitoes can lay eggs in both wet and dry environments.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 23,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Which then leads us to the next 2 factors!",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 24,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "With a lack of proper sanitation, waste will be poorly disposed of, collecting stagnant water that encourages the breeding of mosquitoes.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 25,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Poor drainage and stagnant water occur especially in areas where people are not informed or are aware of.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 26,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Thus they do not take any effort to remove these stagnant waters, creating more breeding ground for mosquitoes!",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 27,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Can you imagine having a mosquito breeding ground near our settlements? It will increase the chances of Malaria spreading!",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 28,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Last but not least, if the area has limited access to healthcare, the delayed treatment for patients who are infected will make it riskier for more to be infected!",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC003",
                "stackNo": 29,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Thankfully, Utopia is a developed country, thus, we generally do not face any of these issues, so we can rest assured...",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Scold your assistant on the spot for being complacent",
                        "impactScene": " Your assistant apologised and went to do more research on Utopia's climate.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 15
                            },
                            {
                                "attributeName": "health",
                                "effect": 15
                            }
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "Agree with your assistant and do nothing",
                        "impactScene": "Your country will suffer from diseases and your reputation decreases as a result.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": -15
                            },
                            {
                                "attributeName": "health",
                                "effect": -15
                            }
                        ]
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 30,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "After some research, I found out that climate, temperature, precipitation and relative humidity all affect the habitat needed to breed mosquitoes!",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 31,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "In our tropical climate, temperatures are usually within the range of 22-degree Celsius to 30-degree Celsius which increases the lifespan of female mosquitoes.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 32,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "This will potentially increase the frequency of bites thus, leading to higher incidence of Malaria.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 33,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Not only that, higher temperatures shorten development time of parasites in the mosquitoes. The mosquito will start infecting hosts earlier and this leads to a higher incidence of Malaria.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Then it seems like our precipitation and relative humidity are suitable for the breeding of mosquitoes as well",
                        "impactScene": "",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 34,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Unfortunately, yes. We have heavy rainfall throughout the year, hence, stagnant water might be washed away, and Utopia will have a lower incidence of Malaria.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 35,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "However, if water stops flowing due to accumulation of debris, it will create stagnant pools, encouraging the breeding of mosquitoes and thus...",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Leading to the higher incidence of Malaria!",
                        "impactScene": "",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 36,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Exactly, and our relative humidity (RH) is within the range of 50-60% which is the RH needed for the survival and activity of mosquitoes.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Well, this definitely sounds like a problem now...",
                        "impactScene": "",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 37,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Can you get me a report on the number of cases of Malaria in Utopia for the last 6 months?",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 38,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Sure! I’ll be right back!",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 39,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "A few minutes later...",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 40,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Minister, here is the report you have requested for.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC003",
                "stackNo": 41,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "With the graph report on number of Malaria cases, you need to make a decision on whether to tackle the problem of Malaria.",
                "characterName": "",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/graphData/MalariaCases.xlsx",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Tackle Malaria",
                        "impactScene": "You have successfully prevented the breeding of mosquitoes, decreasing the cases of Malaria in Utopia.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 20
                            },
                            {
                                "attributeName": "health",
                                "effect": 20
                            }
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "Leave it as it is",
                        "impactScene": "With the climate, with no prevention of the breeding of mosquitoes, the mosquitoes start to breeding rapidly, increasing the cases of Malaria in Utopia.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": -20
                            },
                            {
                                "attributeName": "health",
                                "effect": -20
                            }
                        ]
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 42,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/11881.jpg",
                "description": "Let's zoom out of Utopia and look at how the rest of the world suffered from the impact of Malaria. In 2010, there were 216 million Malaria cases worldwide, leading to 537,000 to 907,000 deaths.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 43,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/11881.jpg",
                "description": "The cases vary but are mostly from LDCs. Congo and Nigeria have more than 40% of the total deaths from Malaria.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 44,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/11881.jpg",
                "description": "Zooming in to Nigeria...",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 45,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/graveyard.jpg",
                "description": "Malaria affected Nigeria's infant mortality rate. With every 1000 children born alive, 140 died from Malaria within the 1st year. Pregnant women infected with the parasite then pass on the parasite to the unborn child leading to death of the child within the 1st year of birth.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 46,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/graveyard.jpg",
                "description": "There are up to 200,000 infant deaths due to Malaria annually... Malaria not only causes deaths, but also economic impacts.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 47,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/graveyard.jpg",
                "description": "Malaria increased the medical expenses of the average household from insecticide-treated mosquito nets, medication and travel expenses for the treatment of Malaria.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 48,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/graveyard.jpg",
                "description": "Families lose income from days off from work for treatment, recovery, cost of implementing preventive measures as well as costs for funerals.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 49,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/graveyard.jpg",
                "description": "The economic burden of Malaria is as high as 34% of a household’s income in the republic of Ghana!",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 50,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": "A few months later...",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 51,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": "There are a team of woodcutters clearing the forests for the construction of new infrastructures.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 52,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": "Did you notice that there are more and more mosquitoes as compared to a few months ago? I’m getting at least a few bites every day!",
                "characterName": "Woodcutter Steve",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/guyCamoPants.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 53,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": "Yes, I've noticed! The clearing of the trees must have provided the mosquitoes with more breeding ground.",
                "characterName": "Woodcutter Ben",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/guyNormal.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 54,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": "Woodcutter Peter suddenly rushes over.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 55,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": "Did you guys hear that Joe got hospitalised after getting bitten by mosquitoes?!",
                "characterName": "Woodcutter Peter",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/guyChinese.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 56,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": "Are you serious? What happened?",
                "characterName": "Woodcutter Ben",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/guyNormal.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 57,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": "I'm not sure, we were only told that he’s currently having symptoms such as headache and vomiting.",
                "characterName": "Woodcutter Peter",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/guyChinese.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 58,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": "OMG.",
                "characterName": "Woodcutter Steve",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/guyCamoPants.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 59,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": "Those sound like the symptoms of Malaria! Maybe he got it from a mosquito bite?",
                "characterName": "Woodcutter Steve",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/guyCamoPants.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 60,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": " ???!!!",
                "characterName": "Woodcutter Ben",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/guyNormal.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 61,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/forest.jpg",
                "description": "I certainly hope we don't get the disease too! I've been getting bitten by mosquitoes for the past few days!",
                "characterName": "Woodcutter Ben",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/guyNormal.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 62,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/breakingNews.jpg",
                "description": "After a few days...",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 63,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/breakingNews.jpg",
                "description": "Breaking news!",
                "characterName": "Reporter",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/reporter.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 64,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/breakingNews.jpg",
                "description": "It is reported that a team of woodcutters caught Malaria while clearing the forests. Is Malaria reemerging in Lion City?",
                "characterName": "Reporter",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/reporter.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 65,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Back at a health discussion with WHO and other countries.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 66,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "There is a re-emergence of Malaria in Lion City.",
                "characterName": "WHO representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "What? How is that possible?",
                        "impactScene": "",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 67,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "It was due to forest clearings in the country, as it creates many breeding grounds for mosquitoes.",
                "characterName": "WHO representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 68,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "The team of woodcutters most probably caught Malaria from the forest clearing.",
                "characterName": "WHO representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Did they not take the anti-malarial drugs?",
                        "impactScene": "",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 69,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "It was reported that they did.",
                "characterName": "WHO representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 70,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "However, the anti-malarial drugs are ineffective.",
                "characterName": "WHO representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC003",
                "stackNo": 71,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Is it because of ...",
                "characterName": "",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "The prolonged usage of counterfeit anti-malarial drugs",
                        "impactScene": "Yes! Both the prolonged usage of counterfeit and incomplete doses of anti-malarial drugs has allowed the surviving Malaria parasites to build resistance to the drug.",
                        "consequences": []
                    },
                    {
                        "optionId": "2",
                        "optionName": "The incomplete doses of anti-malarial drugs",
                        "impactScene": "Yes! Both the prolonged usage of counterfeit and incomplete doses of anti-malarial drugs has allowed the surviving Malaria parasites to build resistance to the drug.",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 72,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Which means?",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 73,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "For example, migrant workers may have injected counterfeit or incomplete doses of anti-Malaria drugs.",
                "characterName": "WHO Representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 74,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Such counterfeit/ incomplete drugs are unable to completely wipe out Malaria parasites in a human body. Hence, leaving surviving Malaria parasites.",
                "characterName": "WHO Representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 75,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "When these migrant workers travel between countries, they bring the Malaria parasites with them.",
                "characterName": "WHO Representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 76,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "When mosquitoes from countries bite these migrant workers, the mosquitoes become infected with Malaria and bites other humans.",
                "characterName": "WHO Representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 77,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Thus, this causes Malaria to spread to other countries.",
                "characterName": "WHO Representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "There’s no way to prevent this then!",
                        "impactScene": "",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 78,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "What about climate change? I assume it would have had an impact on the spread of Malaria too?",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 79,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Yes! With climate change, the temperature at higher altitudes increases, becoming a favourable breeding ground for mosquitoes.",
                "characterName": "WHO Representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 80,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "In fact, climate change has raised the temperatures of Central Highlands in Kenya, thus allowing the breeding of mosquitoes at higher altitudes with 4 million more people at risk of Malaria infection!",
                "characterName": "WHO Representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 81,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Could we have prevented the infected mosquitoes from biting us in the first place? For example, we could use mosquito repellent!",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC003",
                "stackNo": 82,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "What do you think? Perhaps you can implement the solution and observe if it helps to stop the spread of Malaria",
                "characterName": "WHO Representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Yes, distribute mosquito repellent",
                        "impactScene": "No changes to the mosquito population, received some public outlash due to the waste of tax funds.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": -10
                            },
                            {
                                "attributeName": "health",
                                "effect": -10
                            }
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "No, find other ways to stop the spread of Malaria.",
                        "impactScene": "Your team decided to launch an experiment to find out other ways of combating mosquitoes.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 15
                            }
                        ]
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 83,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "A few months later...",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 84,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Repellent used to be effective. However, mosquitoes have built resistance against the chemicals within a short time, even after changing the chemical pesticide from DDT to pyrethroids!",
                "characterName": "WHO Representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 85,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "We need to remind every country about the possibility of Malaria spreading, and to continue taking action to prevent increase in new breeding grounds for mosquitoes!",
                "characterName": "WHO Representative",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 86,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Noted.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/formal1Guy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 87,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "Chapter 2.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 88,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "Back at your residential estate...",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 89,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "Ah Boy, I'm home!",
                "characterName": "Mom",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/mom.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 90,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "...",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 91,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "Hmm... No response?",
                "characterName": "Mom",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/mom.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 92,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "Mom walks to the sofa and sees her son looking uncomfortable and scratching himself.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 93,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "What's wrong Ah Boy?",
                "characterName": "Mom",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/mom.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 94,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "Mom, I'm feeling really itchy and my mouth and tongue feels painful.",
                "characterName": "Ah Boy",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/ahboy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 95,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "Oh no, there's rashes all over your body!",
                "characterName": "Mom",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/mom.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 96,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "Are you allergic to something?",
                "characterName": "Mom",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/mom.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 97,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "I don't know...",
                "characterName": "Ah Boy",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/ahboy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 98,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "What have you touched or eaten just now?",
                "characterName": "Mom",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/mom.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 99,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "I...",
                "characterName": "Ah Boy",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/ahboy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 100,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "I'm home!",
                "characterName": "You",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 101,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "You sense that the mood isn't right.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 102,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "What's wrong with Ah Boy?",
                "characterName": "You",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 103,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "Ah Boy is having rashes and he says that his mouth and tongue are feeling sore.",
                "characterName": "Mom",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/mom.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 104,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "...",
                "characterName": "You",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 105,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "Ah Boy, has any of your friends been unwell?",
                "characterName": "You",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 106,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "Yesterday Joshua was having some rashes when we went out to play.",
                "characterName": "Ah Boy",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/ahboy.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 107,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "???!",
                "characterName": "You",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 108,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "This is bad... There were a few cases of hand foot mouth disease in the community recently!",
                "characterName": "You",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 109,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/residential+state.jpg",
                "description": "We need to call in the doctor.",
                "characterName": "You",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 110,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/room.jpg",
                "description": "In Ah Boy's Room.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 111,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/room.jpg",
                "description": "Doctor, how is Ah Boy?",
                "characterName": "You",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 112,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/room.jpg",
                "description": "With his current symptoms, I highly suspect that it is a case of hand foot mouth disease.",
                "characterName": "Doctor",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/doctor.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 113,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/room.jpg",
                "description": " I would need to take a throat swab test for him.",
                "characterName": "Doctor",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/doctor.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 114,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Back at Ministry of Health Headquarters.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 115,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Let's get the meeting started.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 116,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "For today’s agenda, we will be discussing the direction of the office with regards to handling new diseases. Our minister of health will first start.",
                "characterName": "Assistant",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 117,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "It seems that there are news of a new infectious disease sprouting within our communities.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Does anyone know anything about hand foot mouth disease(HFMD)?",
                        "impactScene": "",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 118,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "From what we know, referencing cases in other countries, HFMD is a disease with seasonal outbreaks all year long.",
                "characterName": "Doctor",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/doctor.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 119,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "It produces painful sores in the mouth, throat and tongue along with rashes on the hands, feet and groin area.",
                "characterName": "Doctor",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/doctor.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 120,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "It is more common among children, but it can also affect adults.",
                "characterName": "Doctor",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/doctor.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Is it very infectious?",
                        "impactScene": "",
                        "consequences": []
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 121,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Yes it is an infectious disease.",
                "characterName": "Doctor",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/doctor.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC003",
                "stackNo": 122,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "So...",
                "characterName": "",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/doctor.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": " It can be transmitted through direct contact with bodily fluids",
                        "impactScene": "Yes, infectious disease can be transmitted through direct contact with bodily fluids.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 15
                            }
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "It cannot be transmitted through direct contact with bodily fluids",
                        "impactScene": "Wrong, infectious disease can be transmitted through direct contact with bodily fluids.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": -15
                            }
                        ]
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 123,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "However, one often recovers in 7-10 days without medical treatment.",
                "characterName": "Doctor",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/doctor.jpg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 124,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "A few days later...",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 125,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "During a meeting with the Prime Minister.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 126,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "With regards to the general health of the citizens, do we have any updates from the health ministry regarding HFMD?",
                "characterName": "Prime Minister",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/pm.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 127,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Yes, we have received some relevant updates.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/pm.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 128,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "You proceed to convey to the prime minister what the doctor has shared previously.",
                "characterName": "",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/pm.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 129,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "We would have to make a decision. Based on the data of number of HFMD cases, what would you suggest?",
                "characterName": "Prime Minister",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/pm.svg",
                "graphImage": "",
                "graphData": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/graphData/HFMDcases.xlsx",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Suggest to lock down some parts of the community.",
                        "impactScene": "There may be some community cases of HFMD, and it is an infectious disease. However, the situation is not so severe to lock down some parts of the community. Furthermore, our country is well equipped to handle these small cases.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": -15
                            }
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "Educate and inform the public",
                        "impactScene": "Prime minister impression of you improved.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 15
                            }
                        ]
                    }
                ]
            },
            {
                "sceneId": "SC002",
                "stackNo": 130,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "I have decided to notify the public on HFMD and its symptoms. Also, inform the schools be on alert. Let's move on to the next agenda of the meeting.",
                "characterName": "Prime Minister",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/pm.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 131,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Alright.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/pm.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 132,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Chapter 3: At the International diabetes federation (IDF) congress.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 133,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "A global platform to discuss a broad range of diabetes issues where physicians, scientists, nurses, educators and other healthcare professionals, as well as government representatives and policy makers, gather.",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 134,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Our ministry has also noticed an increasing trend of diabetes amongst the citizens over the years, and this year the rate is pretty alarming.",
                "characterName": "You",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 135,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Ah... Utopia...",
                "characterName": "IDF director",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 136,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "It has been an issue of many countries for many years.",
                "characterName": "IDF director",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 137,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Beeps..",
                "characterName": "System",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/system.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 138,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "-Explaining diabetes-",
                "characterName": "System",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/system.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 139,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Diabetes is a disease whereby a body is unable to manage sugar.",
                "characterName": "System",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/system.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 140,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Some symptoms of a diabetic human may include frequent thirst and urination, weight loss, desensitized wounds and compromised healing process.",
                "characterName": "System",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/system.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 141,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "IDF director seems to be thinking...",
                "characterName": "",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 142,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Do you know what are the causes attributed to diabetes?",
                "characterName": "IDF director",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 143,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Please make a decision.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Due to westernization of diet and urbanization of lifestyle",
                        "impactScene": "Correct!",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 10
                            },    
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "Due to the wonderful food choices that we have and diabetes is very infectious",
                        "impactScene": "Diabetes indeed is caused by our daily food choices. However, it is not infectious",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": -10
                            },
                        ]
                    }
                ]
            },
            {
                "stackNo": 144,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Our country is so developed, giving our people a comfortable life. Technology is advanced, healthcare is available to all, money, food and transportation are not an issue.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 145,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Comparatively to Cambodia... They seem to be facing issues with diseases as well.",
                "characterName": "IDF director",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Cambodia isn't facing diseases such as Diabetes.",
                        "impactScene": "Low developed countries (LDC) face issues with infectious diseases as well, even if they don’t have as much issues with diabetes.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 10
                            }
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "Cambodia is facing the same diseases as Utopia.",
                        "impactScene": "Low developed countries(LDC) face issues with infectious diseases as well, even if they don’t have as much issues with diabetes",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": -10
                            }
                        ]
                    }
                ]
            },
            {
                "stackNo": 146,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Why do you say so?",
                "characterName": "IDF director",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 147,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Cambodia is a LDC. As such their social, economic and environmental factors differ greatly from us.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 148,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "With regards to the social, economic and environmental factors, developed countries (DC) and low developed countries (LDC) are worlds apart.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 149,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "In DC, people are very well off and can afford many things that people in LDC could not. Food is not an issue in DC.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 150,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "On the contrary, there are many cases of obesity due to overconsumption of food, and obesity often leads to coronary heart disease, which can be fatal and cause death.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 151,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Compared to people in LDC, people are often malnourished because of poverty.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 152,
                "sceneType": "normal",
                "backgroundImage": "",
                "description": "Well then, try to relate this with exercise. Do you think the fitness culture of Utopia is lacking?",
                "characterName": "IDF director ",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Utopia might be lacking in the physical activity",
                        "impactScene": "In terms of physical activity, citizens in DC are lacking far behind as compared to LDC.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 15
                            },
                            {
                                "attributeName": "health",
                                "effect": 15
                            }   
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "Utopia definitely doesn't lose out to other countries for exercise",
                        "impactScene": "In general, the physical activity for the average citizen is lesser in a DC than LDC. Those who are advocates of fitness are an exception.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": -15
                            },
                            {
                                "attributeName": "health",
                                "effect": -15
                            }                         
                        ]
                    }
                ]
            },
            {
                "stackNo": 153,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "A DC tends to be advanced in technology and transportation is not an issue. Manual labour is replaced by machines, meaning that physical labors are greatly reduced.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 154,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "In a LDC, it is the opposite. Productions are often done with physical labor and transportation by foot to places are common.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 155,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "The financial status of the people affects the economy of the country. If the people of the country are poor, the country is not well-off either.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 156,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Without resources, the country is unable to progress and thus restricts the country’s attainment of necessity such as healthcare. Besides these, poverty can affect living conditions, access to safe drinking water and proper sanitation.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 157,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": " Living conditions are poor in LDC as people often live in slums and they have no access to safe drinking water and proper sanitation.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 158,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "If infectious disease were to strike a LDC, death rates will be high as the country does not have the resources to manage the outbreak. For DC, these are not as much of an issue.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 159,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Well, it seems that...",
                "characterName": "IDF director",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Infectious disease is more prominent in DC and degenerative disease is more prominent in LDC.",
                        "impactScene": "I would say that degenerative disease is more prominent in DC and infectious disease is more prominent in LDC.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": -15
                            },
                            {
                                "attributeName": "health",
                                "effect": -15
                            }                            
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "Degenerative disease is more prominent in DC and infectious disease is more prominent in LDC.",
                        "impactScene": "Great observation Director! You are right! Infectious diseases are more prevalent in LDC and Degenerative diseases are more prevalent in DC.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 15
                            },
                            {
                                "attributeName": "health",
                                "effect": 15
                            }                         
                        ]
                    }
                ]
            },
            {
                "stackNo": 160,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Infectious diseases are communicable and contagious, often transmitted by a pathogen which can be bateria, viruses, parasites or fungi. It is spread directly or indirectly through vectors, or from person to person.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 161,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Infectious diseases are deadlier for LDC because they are commonly associated with poverty, poor diet and limited healthcare. LDCs do not have the means to contain such diseases, especially not in their living environment.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 162,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Let me guess...",
                "characterName": "IDF Director",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Infectious disease are HIV/Aids and Alzhemier’s disease",
                        "impactScene": "Well, some examples of infectious disease include lower respiratory infections, Diarrhoeal diseases, HIV/AIDS or Malaria but not Alzhemier’s disease.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 5
                            }
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "Infectious disease are Lower respiratory infections and Malaria",
                        "impactScene": "Yes, you are correct. Some examples of infectious disease include lower respiratory infections, Diarrhoeal diseases, HIV/AIDS or Malaria..",
                        "consequences": [
                            {
                                "attributeName": "health",
                                "effect": 20
                            },
                            {
                                "attributeName": "reputation",
                                "effect": 0
                            }
                        ]
                    }
                ]
            },
            {
                "stackNo": 163,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "As for DC like us, degenerative diseases caught up with us from our lifestyle choices, eating habits, bodily wear and tear or from genetic causes. While they are not contagious, a person’s health condition will gradually break down physiologically.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 164,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Because people in DC are well-off, diet and lifestyle habits are different. People consume food with high calories and sugar content while having low levels of physical activity.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 165,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "I see...",
                "characterName": "IDF Director",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 166,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "It seems that the cause of this nationwide issue is people’s lifestyle habits. We need to do something to change our people’s habits.",
                "characterName": "IDF Director",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "sceneId": "SC002",
                "stackNo": 167,
                "sceneType": "normal",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/meetingroom.jpg",
                "description": "Worry not, I already had a suggestion for that.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/IDF_director.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Promote healthy lifestyle",
                        "impactScene": "Yes, this is a good suggestion. But what can we do to motivate people to have a healthy lifestyle? We will need to start to work on our marketing strategies.",
                        "consequences": [
                            {
                                "attributeName": "health",
                                "effect": 10
                            },
                            {
                                "attributeName": "reputation",
                                "effect": 10
                            }
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "Compulsory requirement of reduced sugar content in food products",
                        "impactScene": "This is a very effective way to limit the sugar intake of our people. But this may cause a strong disagreement from the public. We may want to promote a healthy lifestyle to bring awareness to our people first before implementing such measures.",
                        "consequences": [
                            {
                                "attributeName": "health",
                                "effect": 20
                            },
                            {
                                "attributeName": "reputation",
                                "effect": -10
                            }
                        ]
                    }
                ]
            },
            {
                "stackNo": 168,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Back in the Ministry of health’s office..",
                "characterName": "",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 169,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Hey John! I have something to notify you about!",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 170,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Oh hey, minister, what is it about?",
                "characterName": "Advisor",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 171,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "The prime minister office has decided to implement 2 campaigns to deal with diabetes and HFMD.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 172,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Alright! Time for us to take some action then! What kind of campaigns will the two be on?",
                "characterName": "Advisor",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "My social media campaign",
                        "impactScene": "Your social media followers increase ",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 5
                            }
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": "Education campaign for diabetes and healthy lifestyles",
                        "impactScene": "Many citizens are well informed and are seen taking measures for healthy living and dieting",
                        "consequences": [
                            {
                                "attributeName": "health",
                                "effect": 15
                            },
                            {
                                "attributeName": "reputation",
                                "effect": 10
                            }
                        ]
                    }
                ]
            },
            {
                "stackNo": 173,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "As for HFMD, we will also have educational campaigns to raise awareness on HFMD symptoms and its hygienic practices to prevent the spread. Additionally, we will have schools to adhere to strict hygienic practices like washing hands and be on alert for any suspected HFMD cases.",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 174,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Wonderful! How will we evaluate the success of our campaign in improving the health of our citizens? Do we have any indicators?",
                "characterName": "Advisor",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 175,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Hmmm...",
                "characterName": "You",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": [
                    {
                        "optionId": "1",
                        "optionName": "Calorie intake",
                        "impactScene": "Calorie intake will help to measure the success of diabetes campaigns by indicating the amount of food consumption in general for each citizen.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 5
                            }
                        ]
                    },
                    {
                        "optionId": "2",
                        "optionName": " Life expectancy and Infant mortality",
                        "impactScene": "Life expectancy and Infant mortality is a good indicator of health. Life expectancy is an estimate of how long a person can expect to live while Infant mortality rate refers to the survival of infants till 1 year old.",
                        "consequences": [
                            {
                                "attributeName": "reputation",
                                "effect": 10
                            }
                        ]
                    }
                ]
            },
            {
                "stackNo": 176,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Calorie intake can help to measure overconsumption of underconsumption for an individual. However, with regards to the infections, life expectancy and infant mortality is a better indicator of measurement of health and if the campaigns succeed or not.",
                "characterName": "Health Minister",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Health_Minister.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 177,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Furthermore,  Life expectancy and Infant mortality reflects the general health conditions such as standards of healthcare, access to health care and clean water.",
                "characterName": "Health Minister",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Health_Minister.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 178,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "I see. In that case we will roll out the 2 initiatives as discussed and ensure we improve the health of our citizens",
                "characterName": "Advisor",
                "characterImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/Assistant.svg",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 179,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Health is measured with infant mortality rate, life expectancy and calorie intake. Diseases cause an alarming amount of deaths per year. Diseases are divided into 2 categories: Infectious and degenerative.",
                "characterName": "Game summary",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 180,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Some examples of infectious diseases are Malaria and HFMD.",
                "characterName": "Game summary",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 181,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "LDC and DC encounter these different types of diseases on a different scale due to the social, environmental and economic factors.",
                "characterName": "Game summary",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 182,
                "sceneType": "normal",
                "backgroundImage": "https://drive.google.com/uc?export=view&id=1WTc84eI9WoT_kHJjrDAkWKlpm2_OdoFe",
                "description": "Due to this, the approach to handling diseases are different as well, LDC having lesser resources and infrastructure compared to DC while DCs have  more degenerative diseases due to lifestyle and life expectancy of its citizens.",
                "characterName": "Game summary",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 183,
                "sceneType": "winning",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/shinyNightCity.jpg",
                "description": "From your outstanding leadership, Utopia has flourished and the citizens are in good health. Utopia stand at the top as the envy of many nations with great leadership and thriving citizens.",
                "characterName": "Game summary",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            },
            {
                "stackNo": 184,
                "sceneType": "losing",
                "backgroundImage": "https://bucketsofstuffs.s3.amazonaws.com/hostedImages/burningCity.jpg",
                "description": "Utopia continue to face crisis with handling both infectious and degenerative diseases. The country lives in fear and the leadership of Utopia faces criticism internationally.",
                "characterName": "Game summary",
                "characterImage": "",
                "graphImage": "",
                "graphData": "",
                "options": []
            }
            
        ]
    }

