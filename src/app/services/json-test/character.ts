import {ImageApiResponse} from '../../models/image-api-response';


export const CHARACTER: ImageApiResponse = 



{
    "user": "user001",
    "imageType": "character",
    "images":
[
    {
        "name":  "bernie",
        "url": "http://d2cb9spvw8xnlm.cloudfront.net/character/bernie.png"
    },
    {
        "name": "fullmetal",
        "url": "http://d2cb9spvw8xnlm.cloudfront.net/character/fullmetal.png"
    },
    {
        "name": "goku",
        "url": "http://d2cb9spvw8xnlm.cloudfront.net/character/goku.png"
    },
    {
        "name": "midora",
        "url": "http://d2cb9spvw8xnlm.cloudfront.net/character/midora.png"
    },
    {
        "name": "onepunch",
        "url" : "http://d2cb9spvw8xnlm.cloudfront.net/character/onepunchman.png"
    },
    {
        "name": "sasuke", 
        "url" : "http://d2cb9spvw8xnlm.cloudfront.net/character/uchiha.png"
    },
    {
        "name": "yukki",
        "url" : "http://d2cb9spvw8xnlm.cloudfront.net/character/yuki.png"
    },
    {
        "name": "man",
        "url": "http://d2cb9spvw8xnlm.cloudfront.net/character/IMG_2580.PNG"
    },
    {
        "name": "women",
        "url": "http://d2cb9spvw8xnlm.cloudfront.net/character/IMG_2585.PNG"
    }
]
}