import {ImageApiResponse} from '../../models/image-api-response';


export const GRAPH: ImageApiResponse = 



{
    "user": "user001",
    "imageType": "graph",
    "images": 
[
    {
        "name":  "graph1",
        "url": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/graph/download.png"
    },
    {
        "name": "graph2",
        "url": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/graph/images.png"
    },
    {
        "name": "graph3",
        "url": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/graph/line.png"
    },
    {
        "name": "graph4",
        "url": "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/graph/OverlayBarGraphsExample_01.png"
    },
    {
        "name": "graph5",
        "url" : "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/graph/test1.png"
    },
    {
        "name": "graph6", 
        "url" : "https://otoro-image-gallery.s3-ap-southeast-1.amazonaws.com/graph/plan.png"
    }
]
}