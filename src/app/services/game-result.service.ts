import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import {GameResultPost} from '../models/game-result-api-post'
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

var apiURL = environment.apiUrl; //api general endpoint 

@Injectable({
  providedIn: 'root'
})



export class GameResultService{

  private gameResult = new BehaviorSubject<string>("result");
  gameState = this.gameResult

  constructor(private http: HttpClient) { }
// API POST TO BACKEND
  postGameResult(gameResult: GameResultPost)  {

    this.http.post<GameResultPost>(apiURL + "test/postPlayersAndResults",JSON.stringify(gameResult),{responseType: "json"}).subscribe(data=>{
      console.log(data);
    });
  }
  getGameState(){
    return this.gameState
  }
// UPDATE RESULT STATUS (to result page popup)
  updateResult(result: string){
    this.gameResult.next(result)
  }
}