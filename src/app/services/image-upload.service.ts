import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { getImagesS3Response } from '../models/image-api-response';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';

//dev api - let api = 'https://ea6qo8if2h.execute-api.us-east-1.amazonaws.com/prod/upload-to-s3';

//prod lamba and api and s3 all in sg  
let lambda_api = 'https://pa1hcqcrd7.execute-api.ap-southeast-1.amazonaws.com/prod';


import { environment } from '../../environments/environment';
import { TokenStorageService } from './token-storage.service';


let api = environment.apiUrl;



@Injectable({
  providedIn: 'root'
})


export class ImageUploadService {

  userid: string;

  constructor(private http: HttpClient, private tokenStorageService: TokenStorageService) { 
    this.userid = this.tokenStorageService.getUser().id;

  }

  getpresignedurls(logNamespace: any, fileType: any) {
    let getheaders = new HttpHeaders().set('Accept', 'application/json');
    let params = new HttpParams().set('fileName', logNamespace).set('fileType', fileType);
    return this.http.get<any>('http://localhost:5000/generatepresignedurl', { params: params, headers: getheaders });
  }

  getpresignedurlsAttribute(logNamespace: any, fileType: any) {
    let getheaders = new HttpHeaders().set('Accept', 'application/json');
    let params = new HttpParams().set('fileName', logNamespace).set('fileType', fileType);
    return this.http.get<any>('http://localhost:5000/generatepresignedurl/attribute', { params: params, headers: getheaders });
  }

  
  getpresignedurlsBackground(logNamespace: any, fileType: any) {
    let getheaders = new HttpHeaders().set('Accept', 'application/json');
    let params = new HttpParams().set('fileName', logNamespace).set('fileType', fileType);
    return this.http.get<any>('http://localhost:5000/generatepresignedurl/background', { params: params, headers: getheaders });
  }

  getpresignedurlsCharacter(logNamespace: any, fileType: any) {
    let getheaders = new HttpHeaders().set('Accept', 'application/json');
    let params = new HttpParams().set('fileName', logNamespace).set('fileType', fileType);
    return this.http.get<any>('http://localhost:5000/generatepresignedurl/character', { params: params, headers: getheaders });
  }


  getpresignedurlsGraphData(logNamespace: any, fileType: any) {
    let getheaders = new HttpHeaders().set('Accept', 'application/json');
    let params = new HttpParams().set('fileName', logNamespace).set('fileType', fileType);
    return this.http.get<any>('http://localhost:5000/generatepresignedurl/graphData', { params: params, headers: getheaders });
  }


  uploadfileAWSS3(fileName: any, contenttype: any, file: any, folderType: any) {
    let body =  {
      fileType: contenttype,
      folderType : folderType,
      name : fileName,
      file: file, 
    }
    return this.http.post<any>(lambda_api + '/upload-to-s3', body);

    //act fail : '400'
 
    //const headers = new HttpHeaders({ 'Content-Type': contenttype });
    //const req = new HttpRequest(
    //  'POST',
    //  fileuploadurl,
    //  file,
    //  {
    //    headers: headers, 
    //  });
    //return this.http.request(req);


  }

  uploadToDB(fileUrl: string, userID: string, folderType: string){
    let body =  {
      userId: userID,
      imageType : folderType,
      imageUrl : fileUrl
    }

    return this.http.post(api + 'test/postImage', JSON.stringify(body), { observe: 'response', responseType: 'text'});
  }


  getAllObjectsFromS3():Observable<getImagesS3Response>{
    return this.http.get<getImagesS3Response>(lambda_api + '/get-from-s3');
  }


  getImagesFromDB(){


    //let userid = this.tokenStorageService.getUser().id;
    console.log(this.userid)

    
    return this.http.get(api + `test/getAllImages?userId=${this.userid}`, { observe: 'response', responseType: 'text'});
    
  }


  deleteImageFromS3(body: any){

    console.log(body)

    return this.http.request('delete', lambda_api + '/delete-from-s3', { body: body, observe: 'response', responseType: 'text' } )

  }

  deleteImageFromDB(fileUrl: string, folderType: string){
    let body =  {
      userId: this.tokenStorageService.getUser().id,
      imageType : folderType,
      imageUrl : fileUrl
    }

    console.log(body)

    return this.http.request('delete', api + `test/deleteImage`, { body: JSON.stringify(body), observe: 'response', responseType: 'text' } )

    }




    //getWeatherStatus(lat: number, lon: number):Observable<WeatherApiResponse> {
  //  return this.http.get<WeatherApiResponse>(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${this.apiKey}`)
  //}
  
  //getMovie(params: string){
  //  if (params === '') {
  //    return of([]);
  //  }
  //  return this.http.get(attribute);
  //}

  // Old 
  //uploadfileAWSS3(fileuploadurl: any, contenttype: any, file: any) { 
 
  //  const headers = new HttpHeaders({ 'Content-Type': contenttype });
  //  const req = new HttpRequest(
  //    'PUT',
  //    fileuploadurl,
  //    file,
  //    {
  //      headers: headers, 
  //    });
  //  return this.http.request(req);
  //}






}
