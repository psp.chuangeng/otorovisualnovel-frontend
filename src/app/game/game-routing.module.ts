import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SceneComponent } from './game-components/scene/scene.component';
import { LoginPageComponent } from './game-components/login-page/login-page.component';
import { GameResultsComponent} from './game-components/game-results/game-results.component'
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card'
// import { AppComponent } from '../app.component';
import { CommonModule } from "@angular/common";
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BuilderRoutingModule } from '../builder/builder-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
const routes: Routes = [

  {path: 'game', component: SceneComponent},
  {path: 'startpage', component: LoginPageComponent},
  {path: 'game-results', component: GameResultsComponent}



];

@NgModule({
  declarations: [
    SceneComponent,
    LoginPageComponent,
    GameResultsComponent,
  ],
  imports: [
    RouterModule.forRoot(routes), 
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    CommonModule,
    NgCircleProgressModule.forRoot(),
    ClarityModule,
    ReactiveFormsModule,
    FormsModule,
    BuilderRoutingModule,
    BrowserAnimationsModule
    
    // SceneComponent
    ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [SceneComponent],

})

export class GameRoutingModule { }
