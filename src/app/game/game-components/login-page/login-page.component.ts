import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { GameService } from 'src/app/services/game.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})


export class LoginPageComponent implements OnInit {
  checkoutForm! : FormGroup;
  playerName : string;
  gameCode : string;
  response : any;
  noPlayerName: boolean = false;
  noValidGameCode: boolean = false;
   
  constructor(
    private router:Router, 
    public service: GameService,
    private formBuilder: FormBuilder)
    {
      this.playerName = "";
      this.gameCode = "";
      this.response = "";
    }
    onSuccess(){
      this.service.submitGameCode(this.gameCode);
      this.service.submitPlayerName(this.playerName);
    }
    checkRes(code : any){
        // for live deployment
        var success = false
        // success = this.http.get<GameApiResponse>(`http://localhost:8081/api/test/getStoryByCode?gameCode=${para}`)
      return false
    }
    onSubmit(){
      console.log("started")
      // Process checkout data here
      if (this.checkoutForm.value.name){
        this.noPlayerName = false;
        if (this.checkoutForm.value.codeInput){
          this.playerName = this.checkoutForm.value.name
          this.gameCode = this.checkoutForm.value.codeInput
          this.service.getSceneList(this.gameCode).subscribe((res:any)=>{
            console.log(res.status)
            if(res.status == 200){
              // console.log(res.body)
            this.service.response = JSON.parse(res.body)
            this.service.submitGameCode(this.gameCode);
            this.service.submitPlayerName(this.playerName);
            this.service.call();
            this.router.navigate([`${"game"}`]);
     
            }
          },(err)=>{
            this.noValidGameCode = true
            
          })
          console.log(this.response)
          console.log(typeof this.response)
        }
        else{
          this.noValidGameCode = true
        }
        
      }
      else{
        this.noPlayerName = true
        this.noValidGameCode = true
      } 
      
    }

ngOnInit(): void {
    
  this.checkoutForm = this.formBuilder.group({
    name: ['', Validators.required],
    codeInput: ['', Validators.required]
  });
}

}

