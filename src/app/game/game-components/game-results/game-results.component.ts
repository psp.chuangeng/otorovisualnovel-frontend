import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
import {GameService} from "../../../services/game.service";
import {GameResultService} from "../../../services/game-result.service";
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-game-results',
  templateUrl: './game-results.component.html',
  styleUrls: ['./game-results.component.css']
})
export class GameResultsComponent implements OnInit {
  result:string;
  playerName : string;
  attributeDict: any;

  constructor(private game: GameResultService, private attribute: GameService, private router:Router, public dialog: MatDialog) {
    this.result = "";
    this.playerName = "";
    this.attributeDict = {} 
   }

  ngOnInit(): void {
    this.game.gameState.subscribe(data => this.result = data)
    this.attribute.playerName.subscribe(data => this.playerName = data)
    this.attribute.attributeResult.subscribe(data => this.attributeDict = data)
  }

 
}
