import { Component, OnInit, HostBinding, HostListener, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import {GameService} from "../../../services/game.service";
import {GameResultService} from "../../../services/game-result.service";
import { FormService } from 'src/app/services/form.service';
import { GameResultsComponent } from '../game-results/game-results.component';
import {MatDialog} from '@angular/material/dialog';
import * as XLSX from "xlsx";
import { ChartComponent } from '../../../builder/builder-components/chart/chart.component';
import {ChangeDetectorRef} from '@angular/core';
import {NgZone} from '@angular/core';
import { timer } from 'rxjs';

// animations
import {
  trigger,
  state,
  style,
  animate,
  transition,
  AnimationEvent,
  query, 
  stagger,
  sequence
} from '@angular/animations';
import { hardDriveDisksIcon, twoWayArrowsIconName } from '@cds/core/icon';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
//import { ConsoleReporter } from 'jasmine';
// import { GameResultsComponent } from '../game-results/game-results.component';


declare var anime : any;



//animation for fade in and fade out effect + / - for feedback effect on meter  
const enterTransition = transition(':enter', [
  style({
    opacity: 0
  }),
  animate('1s ease-in', style({
    opacity: 1
  }))
]);

const leaveTrans = transition(':leave', [
  style({
    opacity: 1
  }),
  animate('1s ease-out', style({
    opacity: 0
  }))
])

const fadeIn = trigger('fadeIn', [
  enterTransition
]);

const fadeOut = trigger('fadeOut', [
  leaveTrans
]);




@Component({
  selector: 'app-scene',
  templateUrl: './scene.component.html',
  styleUrls: ['./scene.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [fadeIn, fadeOut],



})



export class SceneComponent implements OnInit, AfterViewInit {

  //public dialog: MatDialog
  constructor( public dialog: MatDialog, private router: Router, private service : GameService, private game: GameResultService, private form: FormService, private cf:ChangeDetectorRef) {
       //this.disableBarChart = true;
       this.openChartModal = false;
       this.attributeShowEffect = [];
       this.showDamageEffect = false; 
       this.message = localStorage.getItem('response');
       console.log(JSON.parse(this.message));
       this.previewName = "";
       
  }
  

  previewName:string;
  message: any = ""; 
  showDamageEffect: boolean;  
  attributeShowEffect: any[];
  sequence : number = 0;
  playerName : string = "";
  gameCode : string = "";
  sceneLength = 0;
  attributeName : any = [];
  attributeDict : {[key: string]:number} = {};
  attributeDescDict : {[key: string]:string} = {};
  winCondition: any = {};
  scenes : any;
  currentScene : any = {};
  impact : string = "";
  impactNext: any = false;
  winLose: any;
  endOfGame : boolean = false;
  // startOfGame : boolean = false;
  resultAttribute : any = {};
  resultAttributeList : any = [];
  resultPost: any;
  clickAudio : any;
  bgAudio : any;
  winAudio : any;
  loseAudio : any;

  //to preload
  nextScene : any = {};
  nextTextDisplayed: string = "";
  nextOptionLength : number = 0;
  nextBackground : string = "";



  open = true;
  graphOpen = false;
  size = "xl"
  tutorialImg : any;
  tutorialButtonText: string = "Next";
  graphButtonText: string = "Plot your Graph"
  
  animDone: boolean = false;
  // SCENE
  background : string = "";
  optionLength : number = 0;
  
  // ANIMATIONS 
  cState: string = 'enter1';
  textWrapper: any = "Hello";
  textWrapper2: any = "Hello";
  text: any = {};
  impactText: any = {};
  textDisplayed: string = "";
  //disableBarChart: boolean;
  openChartModal: boolean;
  
  // characterState() {
  //   this.cState = (this.cState === 'enter1' ? 'enter2' : 'enter1');
  //   //console.log(this.cState);
  // }
 



// GAME PROGRESS
  onClick(){
    this.currentScene.graphData = ""
    this.clickAudio.play();
    this.impactNext = false;
    this.impact= "";  
    this.sequence += 1;
    this.animDone = false;
    // setTimeout(() => this.loadScene(this.sequence));
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    this.loadScene(this.sequence);
    this.loadNextScene(this.sequence);
    // this.loadScene(this.sequence)
    console.log("loaded scene")



    
    // animations
    // this.characterState();
  

   

  }  


  ngAfterViewInit(){
    if (this.impact == ""){
            this.textWrapper = document.getElementsByClassName('an-1');

            // console.log(this.textWrapper[0].innerHTML)

            this.text = this.currentScene.description
            console.log("text will be:")
            console.log(this.text)
            // console.log(this.textWrapper)
            // console.log(this.textWrapper[0])
            // this.textWrapper[0].innerHTML = this.textWrapper[0].textContent.replace(/\S/g, "<span class='letter'>$&</span>");
            this.textWrapper[0].innerHTML = this.text.replace(/\S/g, "<span class='letter'>$&</span>");
            // console.log(this.textWrapper[0].innerHTML)

            anime.timeline({loop: false})
            .add({
                      targets: '.an-1 .letter',
                      scale: [4,1],
                      opacity: [0,1],
                      translateZ: 0,
                      easing: "easeOutExpo",
                      duration: 950,
                      comp: this,
                      delay: (el:any, i:any) => 45*i,
                      complete: (anim:any) => {
                          console.log("animation done")
                          this.changeDetection();
                      }
                    
                      
                  });  
                        
     }
     else {
                this.textWrapper = document.getElementsByClassName('an-2');

                // console.log(this.textWrapper[0].innerHTML)

                this.impactText = this.impact
                console.log("impact text will be:")
                console.log(this.impactText);
                // console.log(this.textWrapper)
                // this.textWrapper[0].innerHTML = this.textWrapper[0].textContent.replace(/\S/g, "<span class='letter'>$&</span>");
                // console.log(this.textWrapper)
                this.textWrapper[0].innerHTML = this.impactText.replace(/\S/g, "<span class='letter'>$&</span>");
            
                // console.log(this.textWrapper[0].innerHTML)
                anime.timeline({loop: false})
                .add({
                  targets: '.an-2 .letter',
                  scale: [4,1],
                  opacity: [0,1],
                  translateZ: 0,
                  easing: "easeOutExpo",
                  duration: 950,
                  delay: (el:any, i:any) => 45*i,
                  complete: function(anim:any) {
                    console.log("animation done")
                    this.animDone = true;
              
                          }
                
                        });
            console.log("Here Impact")
            
     }
     
  }

  changeDetection(){
    this.animDone = true;
    this.cf.detectChanges();

  }   
 
    
//   onAnimateImpact(){

//     this.textWrapper2 = document.getElementsByClassName('an-2');

//     // console.log(this.textWrapper[0].innerHTML)

//     this.impactText = this.impact
//     console.log("impact text will be:")
//     console.log(this.impactText);
//     // this.textWrapper[0].innerHTML = this.textWrapper[0].textContent.replace(/\S/g, "<span class='letter'>$&</span>");
//     console.log(this.textWrapper2)
//     this.textWrapper2.innerHTML = this.impactText.replace(/\S/g, "<span class='letter'>$&</span>");
 
//     console.log(this.textWrapper2.innerHTML)
//     anime2.timeline({loop: false})
//     .add({
//       targets: '.an-2 .letter',
//       scale: [4,1],
//       opacity: [0,1],
//       translateZ: 0,
//       easing: "easeOutExpo",
//       duration: 950,
//       delay: (el:any, i:any) => 10*i
// });
//   }


  tutorialButton(){
    this.sequence += 1;
    // console.log(this.startOfGame)
    this.tutorialImg = this.service.getTutorialImages(this.sequence-1)
    if(this.tutorialImg == null){
      this.openModal();
      this.tutorialButtonText = "Close"
      this.sequence = 1
      

    }
  }
  graphButton(){
    this.graphOpen =!this.graphOpen;
  }


  optionClick(selection: any){
    console.log(selection);

    this.clickAudio.play();
    if (selection.impactScene != "") {
      this.currentScene.graphData = ""

      this.impactNext = true
      this.impact = selection.impactScene
      
      selection.consequences.forEach( (consequence: any) =>{

        console.log(consequence.attributeName, consequence.effect)
        this.attributeShowEffect.push({showName: consequence.attributeName, showEffect: consequence.effect})
        console.log(typeof(consequence.effect))
        console.log(consequence.effect)
        this.attributeDict[consequence.attributeName] += Number(consequence.effect);
      });
      setTimeout(() => this.ngAfterViewInit());
      console.log(this.sequence)

      this.showDamageEffect = true
      console.log(this.attributeShowEffect)

      timer(2000).subscribe(
        val => {
          this.closeShowDamageEffect();
        }
      )

      
    }
    else{
      console.log(this.sequence)
      selection.consequences.forEach( (consequence: any) =>{

        console.log(consequence.attributeName, consequence.effect)
        this.attributeShowEffect.push({showName: consequence.attributeName, showEffect: consequence.effect})

      this.attributeDict[consequence.attributeName] += Number(consequence.effect);
      console.log(typeof(consequence.effect))
      console.log(consequence.effect)
      });
      this.sequence += 1;
      this.loadScene(this.sequence)
      this.loadNextScene(this.sequence);
      setTimeout(() => this.ngAfterViewInit());

      this.showDamageEffect = true
      console.log(this.attributeShowEffect)

      timer(2000).subscribe(
        val => {
          this.closeShowDamageEffect();
        }
      )

    }
    
    
  }

  closeShowDamageEffect(){
    this.showDamageEffect = false
    this.attributeShowEffect = []
  }

  openModal(){
    this.open =!this.open;
    // this.startOfGame = true
  }



  

  ngOnInit(): void {

    console.log(this.message)

    this.service.playerName.subscribe((data: string) => this.previewName = data)


    console.log(this.previewName)

    //&& name == "previewPlayer"
    if (this.previewName == "") { //only preview will go this route 
      //console.log(localStorage.getItem('response'))
      //this.message = localStorage.getItem('response')

      this.message = JSON.parse(this.message);

      this.service.loadFromLocal(this.message)


      this.service.formatSceneList(this.message.scenes)
      this.service.sceneLength = this.message.scenes.length -1;
    
      this.service.storyDetails = this.message.attributes
      //this.sceneLength = this.message.length -1;
      //this.storyDetails = this.message.attributes
    } 

    console.log(this.attributeDict[0])


    this.clickAudio = new Audio();
    this.clickAudio.src = "../../../../assets/soundFX/GUI_Notification 16 1 Audio Extracted.wav";
    this.clickAudio.load();
    
    this.winAudio = new Audio();
    this.winAudio.src = "../../../../assets/soundFX/win.wav";
    this.winAudio.load();
    this.winAudio.volume = 0.6;


    this.loseAudio = new Audio();
    this.loseAudio.src = "../../../../assets/soundFX/Lose.wav";
    this.loseAudio.load();
    this.loseAudio.volume = 0.6;

    
    this.bgAudio = new Audio();
    this.bgAudio.src = "../../../../assets/soundFX/Alston & Ozone - LSDesigns.mp3";
    this.bgAudio.load();
    this.bgAudio.volume = 0.6;
    this.bgAudio.play();
    this.bgAudio.loop = true;
    this.tutorialImg = this.service.getTutorialImages(this.sequence)
    this.retrieveGame();


  }
  goToPage(pageName:string):void{ 
    this.router.navigate([`${pageName}`])
  }



  retrieveGame(): void {
    this.service.gameCode.subscribe((data: string) => this.gameCode = data)
    this.service.playerName.subscribe((data: string) => this.playerName = data)
    this.sceneLength = this.service.sceneLength
    var storyDeets = this.service.storyDetails
    for (let key in storyDeets){
      this.attributeDict[storyDeets[key].attributeName] = 100;
      this.attributeDescDict[storyDeets[key].attributeName] = storyDeets[key].attributeDescription;
      this.winCondition[storyDeets[key].attributeName] = storyDeets[key].winningScore; 
      
    }
    this.sequence = 1
    this.loadScene(this.sequence)
    this.loadNextScene(this.sequence);
    ;
  }
// 15
// 17
  loadScene(para:number){


    if(para == this.sceneLength){
      console.log("entered this loop")
      // this.animDone = true
      this.endOfGame = true
      this.gameResult();
      this.winLose = this.game.getGameState()
      this.bgAudio.pause();
      this.bgAudio.volume = 0;

      if(this.winLose.value == "Victory"){
        console.log("entered this victory loop")
        this.winAudio.play();
        this.currentScene = this.service.getEndScene("winning")
        console.log(this.currentScene)
        this.textDisplayed = this.currentScene.description
        console.log(this.textDisplayed)
        this.optionLength = this.currentScene.options.length
        console.log(this.optionLength)
        this.background = this.currentScene.backgroundImage
        if(this.open == false){
          setTimeout(() => this.ngAfterViewInit());
        // this.onAnimateImpact();
      }  
      }
      else{
        this.loseAudio.play();
        this.currentScene = this.service.getEndScene("losing")
        this.textDisplayed = this.currentScene.description
        this.optionLength = this.currentScene.options.length
        this.background = this.currentScene.backgroundImage  
        if(this.open == false){
          setTimeout(() => this.ngAfterViewInit());
      }
      }

    }
    else{
      this.currentScene = this.service.getWholeScene(para)
      this.textDisplayed = this.currentScene.description
      this.optionLength = this.currentScene.options.length
      this.background = this.currentScene.backgroundImage
      if(this.open == false){
          setTimeout(() => this.ngAfterViewInit());
      }
    }
    
  }


// END GAME MECHANICS
  gameResult(): void {
    this.service.updateAttribute(this.attributeDict)
    this.game.updateResult("Victory")
    for (let key in this.attributeDict){
      if(this.attributeDict[key] < this.winCondition[key]){
        this.game.updateResult("Defeat")
      }
    }
    
    for (let key in this.attributeDict){
      this.resultAttribute["attributeName"] = key;
      this.resultAttribute["score"] = this.attributeDict[key];
      this.resultAttributeList.push(this.resultAttribute);
      this.resultAttribute = {};
      
    }
  }

  openResult(): void {
    // this.gameResult();
    this.resultPost = 
      {
        "gameCode": this.gameCode,
        "playerName": this.playerName, 
        "playerScores": this.resultAttributeList
      }
    this.game.postGameResult(this.resultPost);
    
    this.dialog.open(GameResultsComponent, {width: "500px", height:"500px", backdropClass: 'transparent', disableClose: true });

  }

// HEALTH BAR MECHANICS

  attributeColor(n:any,k:any){
    if (k<=0)
    {
      return "#ffffff00";
    }
    else if (n == 1)
    {
      return "#FF0000";
      
    }
    else if (n == 2)
    {
      return "#c35817";
    }
    else (n == 3)
    {
      return "#96CC7F";
    }
  }


  callExcel(link: string){
    this.excelUrl(link); 
  }

  openChart(link : string) {
    console.log(link);
    // this.excelUrl(link);
		//const dialogRef = this.dialog.open(ChartComponent);
		 this.openChartModal = true;
  

	}
  //excelUrl
  excelUrl(link : string){ //activate the charts here once excel link is drawn in
    console.log(link)
		if (link != "") {
		
		this.form.getExcelDataFromUrl(link).subscribe((res: any) => {


			
			var data = new Uint8Array(res);
			var arr = new Array();
			for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
			var bstr = arr.join("");
				/* Call XLSX */
				var workbook = XLSX.read(bstr, {type:"binary"});


				/* DO SOMETHING WITH workbook HERE */
				var first_sheet_name = workbook.SheetNames[0];
				/* Get worksheet */
				var worksheet = workbook.Sheets[first_sheet_name];
				
        console.log(worksheet)


        let graphLabel =  {'barChart': worksheet.A3 == undefined ? "" : worksheet.A3.v, 'lineChart': worksheet.A5 == undefined ? "" : worksheet.A5.v , 'pieChart': worksheet.A7 == undefined ? "" : worksheet.A7.v, 'boxPlot': worksheet.A9 == undefined ? "" : worksheet.A9.v};

        
        worksheet['!ref'] = "C1:F151";

        
        let x: any[] = XLSX.utils.sheet_to_json(worksheet,{raw:true});


        

				//data manipulation again to serve charts 
				const keys = Object.keys(x[0]);
				//console.log(keys);
				// this.displayedColumns = keys; 
				// this.dataSource = x;
				//this.postsForm.get('excel')?.setValue(x);

        //console.log(x)
				this.form.changeMessage(x, graphLabel); // pass data to chart 
				//this.disableBarChart = false;

        //this.openChart(link);
		    this.openChartModal = true;


		})
	}
		
	}


loadNextScene(para:number) {
  this.nextScene = this.service.getWholeScene(para+1)
  this.nextTextDisplayed = this.nextScene.description
  this.nextOptionLength = this.nextScene.options.length
  this.nextBackground = this.nextScene.backgroundImage
  console.log('preload');
}


}

// make bg more smooth: either by s3 or by angular loading
// explore putting div/how to remove the highlight/select the characters when doubel clicking
// why the graph need click 2 times
// stop modal from opening twice, only post API once
// find out about credits page
// reduce health sounds fx

// animations
// - transition from screen to screen
// - appearance of avatars change to fade
// - fade appearance of option buttons 

// Done: 
// fix healthbar to show extra health if exceed max —>  change to user feedback upon receiving damage / heal
// Left remove function problem for attribute meter - jw

// Show game list, show list of gamecodes per game title, be able to click to see result from gamecode - cg 
// refresh function - cg
// Create another grph data qn - xy
// animations for next button - xy

// create model for json request DONE
// create services to import json DONE
// create fake json for attributes DONE
// "import" json and take out the attriute DONE
// put into classs SceneComponent below DONE
// remove 'next' button when theres no text DONE
// if no avatar, name, render story scenario w textbox only DONE
// if no options, render textbox and avatar DONE
// make meter DONE
// bar interaction DONE
// find a way to make MULTI meter DONE
// how to make multi health bar DONE

// IMAGE LOADING SLOW, OR REAMINING FOR A DELAY WHEN ITS NOT SUPPOSED TO BE THERE
// finish sharing data w components DONE


// GAME
// check for invalid gamecode DONE
// retrieve all game scenes and load from backend
// create winning and ending scene (logic)
// create winning and ending scene (scene.ts)
// create data for graph question
// fix healthbar to show extra health if exceed max
// sfx for victory, for defeat KIV
// balance the game scores to win and lose
// sound effects for health bar damaage and recovery
// back button / logs button KIV 
// Add Labels for graph - cg 
// disable result screen from closing -xy
// disabled multiple times click on end -xy 
//health meter to show additional score that increase/decrease -Xy
//time limit
//increase difficulty  
//smiling render to sad / angry 
//Create to render graph image also 
// balance the game scores to win and lose 
// sound effects for health bar damaage and recovery 
// animations for other elements (avatar, options, background) 
// back button / logs button KIV 
// sfx for victory, for defeat DONE

// BUILDER
// able to activate start game for gamecode DONE
// post latest scene.ts to backend DONE
// fix graph (chagne to clarity instead of mat UI) DONE
// remove some graph features DONE 

// Drop down include empty option - jw
// Builder submit button / next button disabled validation logic --> cg
// Choose either graph image / graph data --> cg / jw 
// Builder prof kam step by step guide PDF - jw / cg - mon  
// Test builder and backend - jw / cg / jy 
//Validate Meter Name (disable Next if Meter Name is same)- cannot have same meter name
//Validate Add Meter (Disable Add Meter is all Meters not filled)



// login page:
// check for invalid gamecode DONE
// create player name variable to parse into other components DONE
// create GET to call scenesList DONE
// something to intro game after login screen DONE


// game:
// css for graph appearance DONE
// research image.load() KIV
// animations
// - transition from screen to screen
// - appearance of avatars change to fade
// - fade appearance of option buttons 
// - loading of text DONE
// integrate player name into game DONE
// line chart
// 
// sound effects for health bar damaage and recovery
// importing the entire game from figma to hardcoded json for testing DONE
// send post request after endgame DONE
// make meter svg more dynamic: only show if exist NA
// back button / logs button KIV 
// back button logic NA
// background music changes when health lvl low KIV
// research ng destroy angular lifescycle for audio KIV
// currently the next button overlaps the text DONE
// sfx for critical health KIV
// Score for meter bar - xy DONE


// Result page
// make victory POP DONE
// sfx for victory, for defeat KIV
// 

// Builder 
// CSS of form
// CSS of graph
