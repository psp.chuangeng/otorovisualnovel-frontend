import { Directive, HostListener, Output, EventEmitter  } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import * as XLSX from "xlsx";


@Directive({
  selector: '[appReadexcel]'
})
export class ReadexcelDirective {
  excelObservable!: Observable<any>;
  @Output() eventEmiiter = new EventEmitter();

  isNumber: boolean[];
  isTitleInit: boolean; 


  constructor() { 
    this.isNumber = []; 
    this.isTitleInit = false; 
  }

  //when directive chnages

  @HostListener("change", ["$event.target"])
  onChange(target: HTMLInputElement) {
    const file = target.files![0];


    if (file.name.match(/.(xlsx)$/i)) {

      //console.log(file); 
      //read file any field
      this.excelObservable = new Observable((subscriber: Subscriber<any>) => {
        this.readFile(file, subscriber);
      });
      //excelObservable.subscribe((d) => {
      //  console.log(d);
      //})
      this.excelObservable.subscribe((d) => {
        this.eventEmiiter.emit(d)
      });
    }
  }

  readFile(file: File, subscriber: Subscriber<any>){
    const fileReader = new FileReader()
    fileReader.readAsArrayBuffer(file);
    fileReader.onload=(e)=>{
      const bufferArray = e.target?.result; //array buffer here
      const wb:XLSX.WorkBook = XLSX.read(bufferArray, {type: 'buffer'});

      console.log(wb)

      const wsname:string = wb.SheetNames[0];

      const ws: XLSX.WorkSheet=wb.Sheets[wsname];

      console.log(ws)
      console.log(ws.A3)
      console.log(ws.A5)
      console.log(ws.A7)
      console.log(ws.A9)


      
      let graphLabel =  {'barChart': ws.A3 == undefined ? "" : ws.A3.v, 'lineChart': ws.A5 == undefined ? "" : ws.A5.v , 'pieChart': ws.A7 == undefined ? "" : ws.A7.v, 'boxPlot': ws.A9 == undefined ? "" : ws.A9.v};

      //from c1 onwards select all data 
      ws['!ref'] = "C1:F151";
      //console.log(ws)

      const data: any[] = XLSX.utils.sheet_to_json(ws);


      //console.log(data)

      //error checking here to see if data contaisn any integer for the 1st row 
      
      //check if array contains contains integer / flaot 
      let keys = Object.keys(data[0]);
      console.log(keys)

      let test_arry: any[] = []
      let final: any[] = []
      for (let i = 0; i < keys.length; i++) {
        data.map((o: any)=> 
        {
          test_arry.push(o[keys[i]])
        }
          )
          final.push(test_arry);
          test_arry = []
        }
      final.map(array => {
        console.log(array)
        this.isNumber.push(array.reduce(function(result: any[], val: any) {
          return result && typeof val === 'number'; //check if the array consist of numbers only 
       }, true));
        //console.log(this.isNumber)
      })

      console.log(this.isNumber)


      //__EMPTY
      //var res = keys.every(function(element) {return typeof element === 'string';});
      //console.log(res)
      keys.map((key: any) => {

        if (key == parseInt(key, 10) || key == "__EMPTY") { //check if title consist of number or empty title 
          //console.log('is an int ')
          this.isTitleInit = true
          //console.log(this.isTitleInit)
        }

      })


      


      if (this.isNumber.includes(false) || this.isTitleInit) {
        //if error , return error
        
        subscriber.next('error');
        subscriber.complete();  
      } else {

        // at the end then end the labels behind before sending off 
        data.push(graphLabel);
        console.log(data)
        subscriber.next(data);
        subscriber.complete();
      }

      this.isNumber = [];
  
    };
  }


  //!ref: "A1:F151"



}
