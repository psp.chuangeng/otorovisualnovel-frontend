# VN-GAME-CLIENT

##Link: 

1. [Staging Link](http://otoro-vn-staging.s3-website-ap-southeast-1.amazonaws.com)

2. [Production Link](https://www.otoro.world/login)


## Documentation:

### Installation

To clone the application:

```bash
git clone https://gitlab.com/psp.chuangeng/otorovisualnovel-frontend.git
```

To install depencies to run the application:

```bash
npm install
```

To run the application:

```bash
ng serve --open
```

### File structure

The folders are split into components, modal & services:

- Components - To create application logic & view

- Modal - To create interfaces for services & components

- Services - To pass data across components & interact with the backend Api
